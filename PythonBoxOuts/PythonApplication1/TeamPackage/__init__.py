'''
Created on Jan 25, 2018

@author: MAmeer
'''
from PlayerPackage import Player

class Team(object):
    #teamId = ""
    #amIHomeTeam
    #playerOnCourt
    #playersOnTeam = []
    
    def __init__(self, teamID = None, amIHomeTeam = None, playersOnTeam = None):
        
        self.teamId = teamID
        self.amIHomeTeam = amIHomeTeam
       
        if playersOnTeam is None:
            pass
        else:
            self.playersOnTeam = []
            
            ''' ERROR checking for less than 5 players '''
            if len(playersOnTeam) is not 5:
                print("Less Than 5 Players On Court")      
                  
            for line in playersOnTeam:
                if(line['xyz'].__len__() == 3):
                    self.playersOnTeam.append(Player(line['playerId'], 
                                                 teamID, 
                                                 line['xyz'][0], 
                                                 line['xyz'][1], 
                                                 line['xyz'][2]))

    
    ''' returns whether or not team is home team '''
    def isHomeTeam(self):
        return self.amIHomeTeam
    
    ''' returns list of Players '''
    def getPlayers(self):
        return self.playersOnTeam
    
    def getTeamID(self):
        return self.teamId
    
    '''Returns player with given playerId, otherwise returns -1'''
    def getPlayer(self, playerID):
        for player in self.playersOnTeam:
            if player.getPlayerID() == playerID:
                return player
        return -1
    
    
    