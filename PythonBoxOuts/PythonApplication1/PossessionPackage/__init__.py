'''
Created on Feb 12, 2018

@author: MAmeer
'''

class Possession:
    '''
    teamIdOffense = ""
    wallClockStart = 0.0
    wallClockEnd = 0.0
    trackingData = []
    
    '''
    def __init__(self, possession, trackingData = None, events = None):
        
        ''' Pull data from JSON element '''
        self.teamIdOffense = possession['teamId']
        self.wallClockStart = possession['wallClockStart']
        self.wallClockEnd = possession['wallClockEnd']
        self.period = possession['period']
        self.possessionId = possession['possessionId']
        self.possessionNum = possession['possessionNum']
        self.ptsScored = possession['ptsScored']
        self.outcome = possession['outcome']
        self.counts = possession['counts']
       
        ''' trackingData list setup '''
        if trackingData is None:
            pass
        
        else:
            self.trackingData = trackingData
    
    
        ''' events setup '''
        if events is None:
            pass
        
        else:
            self.events = events
            
    
    
    def getPeriod(self):
        return self.period
    
    def getPossessionTeam(self):
        return self.teamIdOffense
            
    def getFrameData(self):
        return self.trackingData
    
    #def getPlayerSpeed(self, playerID, timeStamp):
        
    
    