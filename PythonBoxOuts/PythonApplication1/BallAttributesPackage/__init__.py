'''
Created on Mar 16, 2018

@author: MAmeer
'''

class BallAttributes:
    
    def __init__(self):
        
        self.attributes = { "distanceFromHomeBasket": 0.0,
                            "distanceFromAwayBasket": 0.0,
                            "angleFromBasket": 0.0, 
                            "speed": 0.0,
                            "speedFrame3": 0.0,
                            "speedFrame5:": 0.0,
                            "averageSpeed": 0.0,
                            "directionOfMovement": 0.0,
                           }
        
        
    def setAttributes(self, field, data):
            self.attributes[field] = data