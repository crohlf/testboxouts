'''
Created on Jan 25, 2018

@author: MAmeer
'''
from PlayerAttributePackage import PlayerAttributes
import DistanceCalculator

class Player:
    #playerId = 0
    #teamId = ""
    #x = 0.0
    #y = 0.0
    #z = 0.0
       
    def __init__(self, playerId = None, teamId = None, x = None, y = None, z = None):
        self.playerId = playerId
        self.teamId = teamId
        self.x = x
        self.y = y
        self.z = z
        self.attributes = PlayerAttributes()
        self.reboundArea = 0.0
        self.reboundProb = 0.0
        
        
      
    def getPosition(self):
        return [self.x, self.y, self.z]

    
    def setDistanceFromOtherPlayers(self, OpponentsOnCourt):
        
        myPosition = self.getPosition()
        for badGuy in OpponentsOnCourt:
            self.attributes.distanceFromOpponents.append(DistanceCalculator.getDistance2D(myPosition, badGuy.getPosition()))
    
    def setDistanceFromBall(self, positionOfBall):
        self.attributes.distanceFromBall = DistanceCalculator.getDistance2D(self.getPosition(), positionOfBall)
        
    def setDistanceFromBasket(self, positionOfBasket):
        self.attributes.distanceFromBasket = DistanceCalculator.getDistance2D(self.getPosition(), positionOfBasket)
    
