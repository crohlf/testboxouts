''' 
Author: Charlie Rohlf
Create Date: 4/20/2018
Description: Code to test ModelReader Class

'''
from ModelReader import ModelReader

if __name__ == '__main__':

    myReader = ModelReader("C:\TrackingData\Picklesave1")
    myModel = myReader.ReadModelFromDisk()

    print(myModel.predict_proba([[100.,100.]]))