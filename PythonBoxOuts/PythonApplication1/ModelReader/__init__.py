''' 
Author: Charlie Rohlf
Create Date: 4/16/2018
Description: Class to read saved models from disk

'''

import pickle
import sys

class ModelReader:

    def ReadModelFromDisk(self):

        modelToReturn = "Hello World"

        try:

            with open(self.filename, 'rb') as filetoread:
                modelToReturn = pickle.load(filetoread)
                filetoread.close()

        except:
            print("Error: ", sys.exc_info()[0])
            
        return modelToReturn



    def __init__(self, _filename):
        self.filename = _filename
    






