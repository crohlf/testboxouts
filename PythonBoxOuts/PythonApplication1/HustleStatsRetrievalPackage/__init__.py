'''
Author: Joe Boyle
Create Date: 2/21/2018
Description: Class to contain different methods to retreive Hustle Stats data

'''
from DBConfigsPackage import DBProvider
from HustleStatsRetrievalPackage import HustleEventType
from HustleStatsRetrievalPackage import HustleEvent
import sys
import os

class HustleStatRetrieval:

    # Returns the list of hustle stats for a game
    def GetHustleStatsForGame(self, gameId=None):
        hustleStatList = []
        try:
            sql = self.hustleStatsQueryString.format(gameId)
            print(sql)
            cursor = self.db.ExecuteQuery(sql, 'DRIVER')

            while True:
                row = cursor.fetchone()
                if(row == None):
                    break
                hustleType = HustleEventType.HustleEventType[row[10]]
                h = HustleEvent.HustleEvent(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], hustleType)
                hustleStatList.append(h)
        except NameError as e:
            z = e
            print("Error: ", z)
        except:
            print("Error: ", sys.exc_info()[0])
        return hustleStatList

    # returns the list of a single type of hustle stats for a game
    def GetHustleStatForGameByType(self, gameId=None, hustleStatType=None):
        hustleStatList = []

        try:
            fullList = self.GetHustleStatsForGame(gameId)

            for h in fullList:
                if(h.hustleStatType == HustleEventType.HustleEventType[hustleStatType]):
                    hustleStatList.append(h)

        except NameError as e:
            z = e[0]
            print("Error: ", z)
        except ValueError as e:
            z = e[0]
            print("Error: ", z)
        except:
            print("Error: ", sys.exc_info()[0])

        return hustleStatList

    def __init__(self, dbConnectionName=None):
        self.dbConnectionName = dbConnectionName
        self.db = DBProvider(dbConnectionName)
        path = os.path.dirname(os.path.realpath(__file__)) + '\HustleStatsGameQuery.txt'

        with open(path, 'r') as f:
            self.hustleStatsQueryString = f.read().replace('\n', ' ')
            #self.hustleStatsQueryString.replace("str:", " ")
        
