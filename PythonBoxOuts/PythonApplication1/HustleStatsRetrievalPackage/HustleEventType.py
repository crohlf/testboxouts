from enum import Enum


class HustleEventType(Enum):
    CONTESTED_2PT_SHOT = 1
    CONTESTED_3PT_SHOT = 2
    CONTESTED_SHOT = 3
    DEFLECTION = 4
    LOOSE_BALL_RECOVERED = 5
    CHARGE_DRAWN = 6
    SCREEN_ASSIST = 7
    BOX_OUT = 8
    UNKNOWN = 9
