  SELECT a.Game_ID
    , a.Event_Num
    , a.Event_Msg_Type
    , a.Action_Type
    , a.Period
    , a.PC_Time
    , a.Person_ID
    , b.Display_First_Last as Player
    , c.Team_id
    , d.Short_Name as Team
    , CASE a.Event_Msg_Type 
             WHEN 483 THEN CASE a.Action_Type WHEN 2 THEN 'CONTESTED_2PT_SHOT' WHEN 3 THEN 'CONTESTED_3PT_SHOT' ELSE 'CONTESTED_SHOT' END 
             WHEN 484 THEN 'DEFLECTION'
             WHEN 485 THEN 'LOOSE_BALL_RECOVERED'
             WHEN 486 THEN 'CHARGE_DRAWN'
             WHEN 487 THEN 'SCREEN_ASSIST'
             WHEN 669 THEN 'BOX_OUT'
			 ELSE 'UNKNOWN'
           END as 'Type'
    FROM Game_Event_Hustle a 
    JOIN Player_Details b on a.Person_ID = b.Person_id
    JOIN Box_Scores c on a.Person_ID = c.Person_id and a.Game_ID = c.Game_id
    JOIN Team d on c.League_id = d.League_id and c.season_id = d.season_id and c.Team_id = d.Team_id
    LEFT JOIN PersonSC e on a.person_id = e.person_id
    WHERE a.Game_ID = '{}'
    AND a.Event_Msg_Type <> 488
    ORDER BY a.Game_ID, a.Period, a.PC_Time DESC