class HustleEvent:

    def __init__(self, gameId, eventNum, eventMsgType, actionType, period, pcTime, personId, playerName, teamId, teamName, hustleStatType):
        self.gameId = gameId
        self.eventNum = eventNum
        self.eventMsgType = eventMsgType
        self.actionType = actionType
        self.period = period
        self.pcTime = pcTime
        self.gameClock = pcTime / 10.0
        self.personId = personId
        self.playerName = playerName
        self.teamId = teamId
        self.teamName = teamName
        self.hustleStatType = hustleStatType
        self.ssId = None

