'''
Created on Feb 13, 2018

@author: MAmeer
'''

class Basket:
    '''
    x = 0.0
    y = 0.0
    z = 0.0
    
    '''
    
    ''' Takes LIST of basket XYZ then creates object '''
    def __init__(self, teamID, basketLocation = None):
        
        if len(basketLocation) != 3:
            self.x = self.y = self.z = None
        
        else:
            self.x = basketLocation[0]
            self.y = basketLocation[1]
            self.z = basketLocation[2]

        self.offensiveTeamID = teamID
            
    def getBasketPosition(self):
        return [self.x, self.y, self.z]
        