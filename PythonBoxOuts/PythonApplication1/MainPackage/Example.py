'''
Created on April 13, 2018

@author: CRohlf
'''

from JSONLoaderPackage import JSON_Loader
from GamePackage import Game
from FramePackage import Frame
from BallPackage import Ball
from TeamPackage import Team
import numpy as np
import scipy as sp
from sklearn import svm, datasets
from sklearn.model_selection import cross_val_score
import matplotlib.pyplot as plt
import _csv
import pickle
from PossessionPackage import Possession
from BasketsPackage import Basket
from PeriodPackage import Period
import time

    
def LoadAllFrames(listOfTrackingData):
    #INPUT: JSON Tracking File in memory as list
    #OUTPUT: List of Frame Objects
    #TO DO: Create Frames with corresponding Team, Player & Ball objects for each Frame
    listofFrames = []
    
    for line in listOfTrackingData:
        '''Check if Ball has null position '''
        try:
            ballX = line['ball'][0]
            ballY = line['ball'][1]
            ballZ = line['ball'][2]
        
        except:
            ballX = 0
            ballY = 0
            ballZ = 0
            
         
        ''' Should objects be initialized in constructor class instead? ''' 
        newFrame = Frame(line['wallClock'],
                          line['frameIdx'],
                          Ball(ballX, ballY, ballZ),
                          Team(0, True, line['homePlayers']), 
                          Team(1, False, line['awayPlayers']))
        #print(Ball(ballX, ballY, ballZ).distanceFromBasket(Basket([-41.75, 0, 10])))
        listofFrames.append(newFrame)
        
    return listofFrames

def LoadEvents(listOfEventData):
    #INPUT: JSON Events File in memory as list
    #OUTPUT: List of Event Objects
    listOfEvents = []
    
    for event in listOfEventData:
        newEvent = Event(event)
        listOfEvents.append(newEvent)
        
    return listOfEvents

def InitializePossessionsAndLoadFrames(dictOfPossessions, listofFrames):
    #INPUT: JSON Possessions File in memory as dictionary
    #OUTPUT: List of Possessions Objects
    #TO DO: Initialize Possessions and put Frames into correct possessions
    listofPossessions = []
    
    #Load possessions file
    PossessionsFile = dictOfPossessions
    framesToBeSliced = listofFrames
    eventsToBeSliced = listOfEvents
    
    print("Number of Frames = " , len(listofFrames) - 1)
    
    ''' Check every possession '''
    for possession in PossessionsFile:
        
        endClock = possession['wallClockEnd']
        possID = possession['possessionId']
        tempFrames = []
        tempEvents = []
        
        #Loads frames belonging to each possession (by time stamp)
        for index, frame in enumerate(framesToBeSliced):
            if frame.getTimeStamp() == endClock: #Find endClock
                tempFrames = framesToBeSliced[:index] #Slice from beginning to endClock
                framesToBeSliced = framesToBeSliced[index:] #Remove sliced frames so we don't have to search them again
                break
        
        for index, event in enumerate(eventsToBeSliced):
            if event.getEventPossessionID() == possID:
                tempEvents = eventsToBeSliced[:index]
                eventsToBeSliced = eventsToBeSliced[index:]
                break
            
        listofPossessions.append(Possession(possession, tempFrames, tempEvents))
       
    return listofPossessions


def InitializePeriodsAndLoadPossessions(listofPossessions):
    #INPUT: List of Possession objects
    #OUTPUT: List of Period objects
    #TO DO: Initialize Periods and put Frames into correct Possessions
    listofPeriods = []
    maxPeriod = listofPossessions[-1].getPeriod()
    print(maxPeriod)
    
    
    basketHome = Basket([-41.75, 0, 10])
    basketAway = Basket([41.75, 0, 10]) 
    
    '''
        -Calculate basket info
        
        -Create new Period Objects for each period
            then toss possessions into corresponding period (much like frames into possessions)
        
    '''
    for i in range(maxPeriod+1):
        
        tempPossessions = []
        
        if i == 3:
            temp = basketHome
            basketHome = basketAway
            basketAway = temp
        
        for possession in listofPossessions:
            if possession.getPeriod() == i:
                for frame in possession.getFrameData():
                    frame.HomeBasket = basketHome
                    frame.AwayBasket = basketAway
                    frame.possessionTeam = possession.getPossessionTeam()
                tempPossessions.append(possession)
                
        listofPeriods.append(Period(basketHome, basketAway, tempPossessions))
        
     
    return listofPeriods


def FinalizeGame(listofPeriods):
    #INPUT: List of Period objects
    #OUTPUT: Finalized Game Object with all internal objects initialized and completed
    #TO DO: Add periods to game and complete other attributes of Game Object
    myGame = Game(gameID, listofPeriods)
    return myGame

def RunGameLoad():
    
    myTrackingList = []
    myPossessionsDictionary = {}
    myEventsList = []
   
    myLoader = JSON_Loader.JSON_Loader()
    
    fileNameTracking = "C:\TrackingData\HoustonGame\Tracking.jsonl"
    myTrackingList = myLoader.LoadJSONL(fileNameTracking)
    
    #fileNameEvents = "C:\TrackingData\events.jsonl"
    #myEventsList = myLoader.LoadJSONL(fileNameEvents)
    
    #fileNameEvents = "C:\TrackingData\possessions.json"
    #myPossessionsDictionary = myLoader.LoadJSON(fileNameEvents)
    
    iris = datasets.load_iris()
    
    
    #for line in myTrackingList:
    #    toPrint = line['homePlayers'][0]['xyz']
    #    print(toPrint)
        
    for line in myEventsList:
        toPrint = line['eventType']
        print(toPrint)
        
    for item in myPossessionsDictionary:
        for subitem in item:
            print(subitem)

def RunGameLoadMalikVersion():
    start_time = time.time()
    myTrackingList = []
    myPossessionsDictionary = {}
    myEventsList = []
        
    myLoader = JSON_Loader.JSON_Loader()
        
    fileNameTracking = "C:\TrackingData\MiamiGame\Tracking.jsonl"
    myTrackingList = myLoader.LoadJSONL(fileNameTracking)
        
    fileNameEvents = "C:\TrackingData\MiamiGame\EVENTS.jsonl"
    myEventsList = myLoader.LoadJSONL(fileNameEvents)
        
    fileNameEvents = "C:\TrackingData\MiamiGame\POSSESSIONS.json"
    myPossessionsDictionary = myLoader.LoadJSON(fileNameEvents)['possessions']
    gameID = myPossessionsDictionary[0]['gameId']
        
       
    # Load all Objects
    Frames = LoadAllFrames(myTrackingList)
    print("Frames Loaded")
        
    Events = LoadEvents(myEventsList)
    print("Events Loaded")
        
    Possessions = InitializePossessionsAndLoadFrames(myPossessionsDictionary, Frames, Events)
    print("Possessions Loaded")
        
    Periods = InitializePeriodsAndLoadPossessions(Possessions)
    print("Periods Loaded")
        
    DistanceCalculator.BallToBasketDistance(Frames)
        
    #DistanceCalculator.CalculatePlayerFromOpponentAndBallDistance(Frames)
    #SpeedCalculationPackage.CalculateMomentSpeeds(Frames, 3)
        
    Game = FinalizeGame(gameID, Periods)  
    print("Games Loaded")
        
    db = DBProvider('CoreStats')
    print(db.filePath)
    print(db.configuration)
    cursor = db.ExecuteQuery("select * from game where game_id = '0021700909'", 'DRIVER')
    row = cursor.fetchone()
    print(row)
        
    print("***********************************")
    hustle = HustleStatRetrieval('CoreStats')
    hustleTypeEvents = hustle.GetHustleStatForGameByType("0021700909", "BOX_OUT")

    for h in hustleTypeEvents:
        print(h.gameId, ", ", h.eventNum, ", ", h.gameClock, ", ", h.playerName, ", ", h.teamName, ", ", h.hustleStatType.name)
        for event in Events:
            if event.eventNum == h.eventNum and (event.eventType == "SHOT" or event.eventType == "FT"):
                print("Associated with -->", event.eventType, "time:", event.timeStamp)
                    
            
            
    print("--- %s seconds ---" % (time.time() - start_time))


def make_meshgrid(x, y, h=.02):
    """Create a mesh of points to plot in

    Parameters
    ----------
    x: data to base x-axis meshgrid on
    y: data to base y-axis meshgrid on
    h: stepsize for meshgrid, optional

    Returns
    -------
    xx, yy : ndarray
    """
    x_min, x_max = x.min() - 1, x.max() + 1
    y_min, y_max = y.min() - 1, y.max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))
    return xx, yy

def plot_contours(ax, clf, xx, yy, **params):
    #Plot the decision boundaries for a classifier.

    #Parameters
    #----------
    #ax: matplotlib axes object
    #clf: a classifier
    #xx: meshgrid ndarray
    #yy: meshgrid ndarray
    #params: dictionary of params to pass to contourf, optional
    
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    out = ax.contourf(xx, yy, Z, **params)
    return out

def TestMLCode():
    #iris = datasets.load_iris()
    # Take the first two features. We could avoid this by using a two-dim dataset
    #X = iris.data[:, :2]
    #y = iris.target
    
    #csvData = []
    #targets = []
    
     
    filename = 'C:\TrackingData\DefenseDataOnlySmall.csv'
    raw_data = open(filename, 'rt')
    reader = _csv.reader(raw_data, delimiter=',', quoting=_csv.QUOTE_NONE)
    #headers = next(reader)
    xlist = list(reader)
    xlist.pop(0)
    data = np.array(xlist).astype('float')
    print(data.shape)
   
    X = data[:, :2]
    y = data[:, 12]
    

    C = 1.0
    
    singlemodel = svm.SVC(kernel = 'poly')
    
    #scores = cross_val_score(singlemodel, X, y, cv=5)
    
    
    singlemodel.fit(X,y)
    
    print(singlemodel.predict([[6.,1.]]))
    
    filenamecounter = 1
    filenameforsave = "C:\TrackingData\Picklesave"
    
    with open(filenameforsave + str(filenamecounter), 'wb') as filetowrite:
        pickle.dump(singlemodel, filetowrite)
        
    print(singlemodel.predict([[1800.,1000.]]))

    with open (filenameforsave + str(filenamecounter), 'rb') as filetoread:
        secondmodel = pickle.load(filetoread)
    
    print(secondmodel.predict([[1800.,1000.]]))
        
        
    
    
    # we create an instance of SVM and fit out data. We do not scale our
    # data since we want to plot the support vectors
    #C = 1.0  # SVM regularization parameter
    #models = (svm.SVC(kernel='linear', C=C),
    #          svm.LinearSVC(C=C),
    #          svm.SVC(kernel='rbf', gamma=1.0, C=C),
    #          svm.SVC(kernel='poly', degree=6, C=C))
    #models = (svm.SVC(kernel='linear', C=C),
    #          svm.LinearSVC(C=C))
    #models = (clf.fit(X, y) for clf in models)
    
    
    
    # title for the plots
    #===========================================================================
    # titles = ('SVC with linear kernel',
    #           'LinearSVC (linear kernel)',
    #           'SVC with RBF kernel',
    #           'SVC with polynomial (degree 3) kernel')
    # 
    # # Set-up 2x2 grid for plotting.
    # fig, sub = plt.subplots(2, 2)
    # plt.subplots_adjust(wspace=0.4, hspace=0.4)
    # 
    # X0, X1 = X[:, 0], X[:, 1]
    # xx, yy = make_meshgrid(X0, X1)
    # 
    # for clf, title, ax in zip(models, titles, sub.flatten()):
    #     plot_contours(ax, clf, xx, yy,
    #                   cmap=plt.cm.coolwarm, alpha=0.8)
    #     ax.scatter(X0, X1, c=y, cmap=plt.cm.coolwarm, s=20, edgecolors='k')
    #     ax.set_xlim(xx.min(), xx.max())
    #     ax.set_ylim(yy.min(), yy.max())
    #     ax.set_xlabel('Sepal length')
    #     ax.set_ylabel('Sepal width')
    #     ax.set_xticks(())
    #     ax.set_yticks(())
    #     ax.set_title(title)
    # 
    # plt.show()
    #===========================================================================
    
    


    print("Hello World!")
 

if __name__ == '__main__':
    
            
    myTrackingList = []
    myPossessionsDictionary = {}
    myEventsList = []
    
    myLoader = JSON_Loader.JSON_Loader()
    
    fileNameTracking = "C:\TrackingData\HoustonGame\Tracking.jsonl"
    myTrackingList = myLoader.LoadJSONL(fileNameTracking)
    
    fileNameEvents = "C:\TrackingData\HoustonGame\events.jsonl"
    myEventsList = myLoader.LoadJSONL(fileNameEvents)
    
    fileNameEvents = "C:\TrackingData\HoustonGame\possessions.json"
    myPossessionsDictionary = myLoader.LoadJSON(fileNameEvents)['possessions']
    
   
    #Load Frames into list
    Frames = LoadAllFrames(myTrackingList)
    print("Frames Loaded")
    
    maxX = 0
    minX = 0
    maxY = 0
    minY = 0
    
    for item in myTrackingList:
        if item['ball'].__len__() == 3:
            if item['ball'][0] > maxX:
                maxX = item['ball'][0]
            if item['ball'][0] < minX:
                minX = item['ball'][0]
            if item['ball'][1] > maxY:
                maxY = item['ball'][1]
            if item['ball'][1] < minY:
                minY = item['ball'][1]
            
    print(maxX, minX, maxY, minY)
    
    start = time.time()
    Possessions = InitializePossessionsAndLoadFrames(myPossessionsDictionary, Frames)
    end = time.time()
    print(end - start)
    print("Possessions Loaded")
    
    #Printing out Frame Data
    #for frame in Frames:
    #    print("p1 = ", frame.teamHome.playersOnTeam[0])
    #    print("p2 = ", frame.teamHome.playersOnTeam[1])
    #    print("p3 = ", frame.teamHome.playersOnTeam[2])
    #    print("p4 = ", frame.teamHome.playersOnTeam[3])
    #    print("p5 = ", frame.teamHome.playersOnTeam[4])
    #    print(" ")
    
    TestMLCode()
        
    for line in myEventsList:
        toPrint = line['eventType']
        print(toPrint)
        
    for item in myPossessionsDictionary:
        for subitem in item:
            print(subitem)
            
            
        
    
   

            
            
        
    
   
