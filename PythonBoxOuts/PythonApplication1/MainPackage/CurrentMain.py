'''
Created on April 13, 2018

@author: CRohlf
'''

from JSONLoaderPackage import JSON_Loader
from GamePackage import Game
from FramePackage import Frame
from BallPackage import Ball
from TeamPackage import Team
from EventPackage import Event
from DBConfigsPackage import DBProvider
from HustleStatsRetrievalPackage import HustleStatRetrieval
from PossessionPackage import Possession
from BasketsPackage import Basket
from PeriodPackage import Period
import sys
import math
import DistanceCalculator
import AngleCalculator
import time
import SpeedCalculator
import numpy as np
import scipy as sp
from scipy.spatial import Voronoi, voronoi_plot_2d, ConvexHull
from sklearn import svm, datasets
from sklearn.model_selection import cross_val_score, GridSearchCV
from sklearn.metrics import make_scorer, roc_auc_score
from sklearn.ensemble import IsolationForest
#import matplotlib.pyplot as plt
import _csv
import csv
import pickle
from GameInfo import GameInfo
from ReboundData import ReboundData
from BoxOutCandidate import BoxOutCandidate
from BoxOutCandidateAttributes import BoxOutCandidateAttributes
from scipy.fftpack import fft
from scipy.signal import savgol_filter
import os

def InitializeWallClockLimit():
    global WALL_CLOCK_LIMIT
    WALL_CLOCK_LIMIT = 1517447319447


def SetGlobalCandidateThresholds():
    global NUMBER_OF_FRAMES_WRESTLING_TO_QUALIFY_AS_CANDIDATE
    NUMBER_OF_FRAMES_WRESTLING_TO_QUALIFY_AS_CANDIDATE = 15

    global MIN_DISTANCE_BETWEEN_PLAYERS
    MIN_DISTANCE_BETWEEN_PLAYERS = 4

    global MAX_DISTANCE_TO_BE_REBOUNDING
    MAX_DISTANCE_TO_BE_REBOUNDING = 18

    global MIN_DISTANCE_TO_BE_CRASHING
    MIN_DISTANCE_TO_BE_CRASHING = 22

    global MAX_ANGLE_TO_BE_CROSSING_PLAYER_REBOUND
    MAX_ANGLE_TO_BE_CROSSING_PLAYER_REBOUND = 45

    global MIN_X_DISTANCE_TO_LEAK_OUT
    MIN_X_DISTANCE_TO_LEAK_OUT = 15

    global MIN_X_LOCATION_TO_LEAK_OUT
    MIN_X_LOCATION_TO_LEAK_OUT = 25

    
def LoadAllFrames(listOfTrackingData):
    #INPUT: JSON Tracking File in memory as list
    #OUTPUT: List of Frame Objects
    #TO DO: Create Frames with corresponding Team, Player & Ball objects for each Frame
    listofFrames = []
    
    for line in listOfTrackingData:
        '''Check if Ball has null position '''
        try:
            ballX = line['ball'][0]
            ballY = line['ball'][1]
            ballZ = line['ball'][2]
        
        except:
            ballX = 0
            ballY = 0
            ballZ = 0
            
         
        ''' Should objects be initialized in constructor class instead? ''' 
        newFrame = Frame(line['wallClock'],
                          line['frameIdx'],
                          Ball(ballX, ballY, ballZ),
                          Team(thisGameInfo._homeTeamID, True, line['homePlayers']),  #Hardcoding until we have Game Info Setup
                          Team(thisGameInfo._awayTeamID, False, line['awayPlayers']), #Hardcoding until we have Game Info Setup
                          line['gameClock'],
                          line['period']) 
        #print(Ball(ballX, ballY, ballZ).distanceFromBasket(Basket([-41.75, 0, 10])))
        listofFrames.append(newFrame)
        
    return listofFrames

def LoadEvents(listOfEventData):
    #INPUT: JSON Events File in memory as list
    #OUTPUT: List of Event Objects
    listOfEvents = []
    
    for event in listOfEventData:
        newEvent = Event(event)
        listOfEvents.append(newEvent)
        
    return listOfEvents

def InitializePossessionsAndLoadFrames(dictOfPossessions, listofFrames, listOfEvents):
    #INPUT: JSON Possessions File in memory as dictionary
    #OUTPUT: List of Possessions Objects
    #TO DO: Initialize Possessions and put Frames into correct possessions
    listofPossessions = []
    
    #Load possessions file
    PossessionsFile = dictOfPossessions
    framesToBeSliced = listofFrames
    eventsToBeSliced = listOfEvents
    
    print("Number of Frames = " , len(listofFrames) - 1)
    
    ''' Check every possession '''
    for possession in PossessionsFile:
        
        endClock = possession['wallClockEnd']
        possID = possession['possessionId']
        tempFrames = []
        tempEvents = []
        
        #Loads frames belonging to each possession (by time stamp)
        for index, frame in enumerate(framesToBeSliced):
            if frame.getTimeStamp() == endClock: #Find endClock
                
                if index < (len(framesToBeSliced) - 1):
                    endPoint = index + 1
                else:
                    endPoint = index

                tempFrames = framesToBeSliced[:endPoint] #Slice from beginning to endClock
                framesToBeSliced = framesToBeSliced[index:] #Remove sliced frames so we don't have to search them again
                break
        
        for index, event in enumerate(eventsToBeSliced):
            if event.getEventPossessionID() == possID:
                #tempEvents = eventsToBeSliced[:index]
                tempEvents.append(event)
                #eventsToBeSliced = eventsToBeSliced[index:]
                #break
            
        listofPossessions.append(Possession(possession, tempFrames, tempEvents))
       
    return listofPossessions


def InitializePeriodsAndLoadPossessions(listofPossessions):
    #INPUT: List of Possession objects
    #OUTPUT: List of Period objects
    #TO DO: Initialize Periods and put Frames into correct Possessions
    listofPeriods = []
    maxPeriod = listofPossessions[-1].getPeriod()
    print(maxPeriod)

    
    for poss in listofPossessions:
        foundShot = False
        for event in poss.events:
            if event.eventType == 'SHOT':
                shotTime = event.timeStamp
                for singleFrame in poss.trackingData:
                    if singleFrame.timeStamp == shotTime:
                        if singleFrame.ballAtThisFrame.x < 0:
                            
                            basketFirst = Basket('', [-41.75, 0, 10])
                            basketSecond = Basket('', [41.75, 0, 10]) 
                        else:
                            
                            basketFirst = Basket('', [41.75, 0, 10])
                            basketSecond = Basket('', [-41.75, 0, 10]) 
                
                        if poss.teamIdOffense == thisGameInfo._homeTeamID:
                            basketFirst.offensiveTeamID = thisGameInfo._homeTeamID
                            basketSecond.offensiveTeamID = thisGameInfo._awayTeamID
                            
                            basketHomeFirstHalf = basketFirst
                            basketAwayFirstHalf = basketSecond

                            basketHomeSecondHalf = Basket(thisGameInfo._homeTeamID, [basketSecond.x, basketSecond.y, basketSecond.z])
                            basketAwaySecondHalf = Basket(thisGameInfo._awayTeamID, [basketFirst.x, basketFirst.y, basketFirst.z])
                            
                            foundShot = True
                            break
                        else:
                            basketFirst.offensiveTeamID = thisGameInfo._awayTeamID
                            basketSecond.offensiveTeamID = thisGameInfo._homeTeamID
                            
                            basketAwayFirstHalf = basketFirst
                            basketHomeFirstHalf = basketSecond
                            
                            basketHomeSecondHalf = Basket(thisGameInfo._homeTeamID, [basketFirst.x, basketFirst.y, basketFirst.z])
                            basketAwaySecondHalf = Basket(thisGameInfo._awayTeamID, [basketSecond.x, basketSecond.y, basketSecond.z])

                            foundShot = True
                            break
            if foundShot:
                break
        if foundShot:
            break

    #basketHome = Basket(thisGameInfo._homeTeamID, [-41.75, 0, 10])
    #basketAway = Basket(thisGameInfo._awayTeamID, [41.75, 0, 10]) 
    
    '''
        -Calculate basket info
        
        -Create new Period Objects for each period
            then toss possessions into corresponding period (much like frames into possessions)
        
    '''
    for i in range(maxPeriod+1):
        
        tempPossessions = []
        
        if i < 3:
            
            for possession in listofPossessions:
                if possession.getPeriod() == i:
                    for frame in possession.getFrameData():
                        frame.HomeBasket = basketHomeFirstHalf
                        frame.AwayBasket = basketAwayFirstHalf
                        frame.possessionTeam = possession.getPossessionTeam()
                    tempPossessions.append(possession)
            
            listofPeriods.append(Period(basketHomeFirstHalf, basketAwayFirstHalf, tempPossessions))
        
        else:
            
            for possession in listofPossessions:
                if possession.getPeriod() == i:
                    for frame in possession.getFrameData():
                        frame.HomeBasket = basketHomeSecondHalf
                        frame.AwayBasket = basketAwaySecondHalf
                        frame.possessionTeam = possession.getPossessionTeam()
                    tempPossessions.append(possession)
            
            listofPeriods.append(Period(basketHomeSecondHalf, basketAwaySecondHalf, tempPossessions))
        
         
    return listofPeriods


def FinalizeGame(gameID, listofPeriods):
    #INPUT: List of Period objects
    #OUTPUT: Finalized Game Object with all internal objects initialized and completed
    #TO DO: Add periods to game and complete other attributes of Game Object
    myGame = Game(gameID, listofPeriods)
    return myGame

def FindPlayerLastNameByID(playerID, playerList):
    for currentPlayer in playerList:
        if currentPlayer['id'] == playerID:
            return currentPlayer['lastName']

    return "Not Found"

def NbaPlayerIdSSMap(personId, playerDictionary):
    for player in playerDictionary:
        if player['nbaId'] == str(personId):
            return player['id']
        
    return print("ERROR: Player NBA ID Not Found")

def InitializeGameInfo():

    #***Section for copy/paste for training set creation***
    #gameID = 'dceda5d2-a499-4a85-91c7-1e387e39e28b'
    #gameIDNBA = '0041700161'
    #gameDate = '1/1/11'
    #homeTeam = 'feb4d7b8-89ef-11e6-80f2-a45e60e298d3'
    #awayTeam = 'feb4d4f0-89ef-11e6-bb54-a45e60e298d3'
    #***End copy/paste section

    gameID = '6fe2ed8e-55f1-43c9-8127-580e959d9497'
    gameIDNBA = '0021700757'
    gameDate = '1/1/11'
    homeTeam = 'feb4ceb0-89ef-11e6-9f6d-a45e60e298d3'
    awayTeam = 'feb4d380-89ef-11e6-9eb6-a45e60e298d3'

    global thisGameInfo 
    thisGameInfo = GameInfo(gameID, gameIDNBA, gameDate, homeTeam, awayTeam)

def LoadTrackingGameData():
   
    
    myTrackingList = []
    myPossessionsDictionary = {}
    myEventsList = [] 
    
        
    myLoader = JSON_Loader.JSON_Loader() 


    #***Section for copy/paste for training set creation***
    fileNameTracking = "C:\TrackingData\SecondTrainingGames\\NOPPOR\Tracking.jsonl"
    fileNameEvents = "C:\TrackingData\SecondTrainingGames\\NOPPOR\Events.jsonl"
    fileNamePoss = "C:\TrackingData\SecondTrainingGames\\NOPPOR\Poss.json" 
    fileNameForOutput = "C:\TrackingData\SecondTrainingGames\\NOPPOR\TestCSV.csv"
    #***End copy/paste section

    
    #fileNameTracking = "C:\TrackingData\SanAntonioGame\Tracking.jsonl" 
    #fileNameTracking = "C:\TrackingData\MiamiGame\Tracking.jsonl" 
    fileNameTracking = "C:\TrackingData\MiamiGame\TrackingShort.jsonl" 
    #fileNameTracking = "C:\TrackingData\MiamiGame\TrackingFirst.jsonl" 
    myTrackingList = myLoader.LoadJSONL(fileNameTracking) 
    
    #fileNameEvents = "C:\TrackingData\SanAntonioGame\EVENTS.jsonl"
    #fileNameEvents = "C:\TrackingData\MiamiGame\EVENTS.jsonl"
    fileNameEvents = "C:\TrackingData\MiamiGame\EVENTSSHORT.jsonl" 
    #fileNameEvents = "C:\TrackingData\MiamiGame\EVENTSFIRST.jsonl"
    myEventsList = myLoader.LoadJSONL(fileNameEvents) 
    
    #fileNamePoss = "C:\TrackingData\SanAntonioGame\POSSESSIONS.json" 
    #fileNamePoss = "C:\TrackingData\MiamiGame\POSSESSIONS.json" 
    fileNamePoss = "C:\TrackingData\MiamiGame\POSSESSIONSSHORT.json" 
    #fileNamePoss = "C:\TrackingData\MiamiGame\POSSESSIONSFIRST.json" 

    fileNameForOutput = "C:\TrackingData\MiamiGame\TestCSV.csv"

    myPossessionsDictionary = myLoader.LoadJSON(fileNamePoss)['possessions']
    gameID = myPossessionsDictionary[0]['gameId'] 
        
    fileNamePlayers = "C:\TrackingData\MiamiGame\players.json" 
    myPlayerDictionary = myLoader.LoadJSON(fileNamePlayers)['players']
       
    # Load all Objects
    Frames = LoadAllFrames(myTrackingList) 
    print("Frames Loaded")  

        
    Events = LoadEvents(myEventsList) 
    print("Events Loaded") 
        
    Possessions = InitializePossessionsAndLoadFrames(myPossessionsDictionary, Frames, Events) 
    print("Possessions Loaded") 
        
    Periods = InitializePeriodsAndLoadPossessions(Possessions)
    print("Periods Loaded")
        
    DistanceCalculator.BallToBasketDistance(Frames)

    SpeedCalculator.CalculateMomentSpeeds(Frames, 3)
        
    thisGame = FinalizeGame(gameID, Periods)  

    #SaveGameObjectPickle(thisGame)

    #TestVoronoi(thisGame)

    DoBoxOutWork(thisGame, fileNameForOutput)   

def SaveGameObjectPickle(gameToSave):
    
    filenameforsave = "C:\TrackingData\PickleSaves\PickleSaveFullGameNOP"
    
    saveTime = time.time()

    for index, singlePeriod in enumerate(gameToSave.gamePeriods):

        with open(filenameforsave + str(index), 'wb') as filetowrite:
            pickle.dump(singlePeriod, filetowrite)

    print("File Save Time:")
    print(time.time() - saveTime)

def RemoveNonCandidates(candidateList):
    listToReturn = []
    for candidate in candidateList:
        if candidate._toBeKeptInList: listToReturn.append(candidate)

    return listToReturn

def CountCloserOpponents(rebounder, otherTeam):
    
    countOfCloserOpponents = 0

    for opposingPlayer in otherTeam:
        if opposingPlayer.attributes.reboundAttributes._distanceToReboundLocation < rebounder.attributes.reboundAttributes._distanceToReboundLocation:
            if abs(opposingPlayer.attributes.reboundAttributes._angleFromReboundLocation - rebounder.attributes.reboundAttributes._angleFromReboundLocation) < MAX_ANGLE_TO_BE_CROSSING_PLAYER_REBOUND:
                countOfCloserOpponents = countOfCloserOpponents + 1

    return countOfCloserOpponents


def CountCrossedOpponents(thisRebound):
    for singlePlayer in thisRebound._attributes._listPlayersFromBasketatShot:

        if singlePlayer.playerId == thisRebound._rebEvent.playerId:

            if singlePlayer.teamId == thisGameInfo._homeTeamID:
                singlePlayer.attributes.reboundAttributes._numberOfCloserOpponents = CountCloserOpponents(singlePlayer, thisRebound._shotFrame.teamAway.playersOnTeam)
            else:
                singlePlayer.attributes.reboundAttributes._numberOfCloserOpponents = CountCloserOpponents(singlePlayer, thisRebound._shotFrame.teamHome.playersOnTeam)

def CalculateIfReboundInOpponentArea(thisRebound):
        
    if thisRebound._rebEvent.teamId != thisRebound._attributes._listPlayersFromRebLoc[0].teamId:
        
        thisRebound._attributes._wasReboundedInOppArea = True

        for singlePlayer in thisRebound._attributes._listPlayersFromBasketatReb:

            if singlePlayer.playerId == thisRebound._rebEvent.playerId:
                singlePlayer.attributes.reboundAttributes._gotReboundinOpponentArea = True
                break


def CalculateListofPlayersSortedByDistanceToBasket(thisFrame):
    sortedListofPlayersToBeReturned = []

    sortedListofPlayersToBeReturned = (sorted(thisFrame.teamHome.playersOnTeam, key=lambda Player: Player.attributes.distanceFromBasket)) + (sorted(thisFrame.teamAway.playersOnTeam, key=lambda Player: Player.attributes.distanceFromBasket))

    sortedListofPlayersToBeReturned = sorted(sortedListofPlayersToBeReturned, key=lambda Player: Player.attributes.distanceFromBasket)

    return sortedListofPlayersToBeReturned


def CalculateListofPlayersSortedByDistanceToRebLoc(thisRebound):
    sortedListofPlayersToBeReturned = []

    sortedListofPlayersToBeReturned = (sorted(thisRebound._shotFrame.teamHome.playersOnTeam, key=lambda Player: Player.attributes.reboundAttributes._distanceToReboundLocation)) + (sorted(thisRebound._shotFrame.teamAway.playersOnTeam, key=lambda Player: Player.attributes.reboundAttributes._distanceToReboundLocation))

    sortedListofPlayersToBeReturned = sorted(sortedListofPlayersToBeReturned, key=lambda Player: Player.attributes.reboundAttributes._distanceToReboundLocation)

    return sortedListofPlayersToBeReturned


def CountNumberOfPlayersRebounding(thisRebound):
    for singlePlayer in thisRebound._attributes._listPlayersFromBasketatReb:
        if singlePlayer.attributes.distanceFromBasket < MAX_DISTANCE_TO_BE_REBOUNDING:
            
            singlePlayer.attributes.reboundAttributes._triedForRebound = True #Figure out why this might be set for everyone

            if singlePlayer.teamId == thisGameInfo._homeTeamID:
                thisRebound._attributes._numberOfHomePlayersTryingForReb = thisRebound._attributes._numberOfHomePlayersTryingForReb + 1
            else:
                thisRebound._attributes._numberOfAwayPlayersTryingForReb = thisRebound._attributes._numberOfAwayPlayersTryingForReb + 1

    if thisRebound._rebEvent.teamId == thisRebound._shotEvent.teamId:
        if thisRebound._rebEvent.teamId == thisGameInfo._homeTeamID:
            thisRebound._attributes._numberOfOffPlayersTryingForReb = thisRebound._attributes._numberOfHomePlayersTryingForReb
            thisRebound._attributes._numberOfDefPlayersTryingForReb = thisRebound._attributes._numberOfAwayPlayersTryingForReb
        else:
            thisRebound._attributes._numberOfOffPlayersTryingForReb = thisRebound._attributes._numberOfAwayPlayersTryingForReb
            thisRebound._attributes._numberOfDefPlayersTryingForReb = thisRebound._attributes._numberOfHomePlayersTryingForReb
    else:
        if thisRebound._rebEvent.teamId == thisGameInfo._homeTeamID:
            thisRebound._attributes._numberOfOffPlayersTryingForReb = thisRebound._attributes._numberOfAwayPlayersTryingForReb
            thisRebound._attributes._numberOfDefPlayersTryingForReb = thisRebound._attributes._numberOfHomePlayersTryingForReb
        else:
            thisRebound._attributes._numberOfOffPlayersTryingForReb = thisRebound._attributes._numberOfHomePlayersTryingForReb
            thisRebound._attributes._numberOfDefPlayersTryingForReb = thisRebound._attributes._numberOfAwayPlayersTryingForReb



def CountPlayersCrashingForRebound(thisRebound): #Also calculating distance travelled & change in area controlled for rebound in same nested loops
    for singlePlayerReb in thisRebound._attributes._listPlayersFromBasketatReb:
        for singlePlayerShot in thisRebound._attributes._listPlayersFromBasketatShot:

            if singlePlayerReb.playerId == singlePlayerShot.playerId: 
                
                singlePlayerReb.attributes.reboundAttributes._distanceTravelledToGetRebound = DistanceCalculator.getDistance2D(singlePlayerReb.getPosition(), singlePlayerShot.getPosition())
                singlePlayerReb.attributes.reboundAttributes._changeInSquareFootageControlled = singlePlayerReb.reboundArea - singlePlayerShot.reboundArea
                singlePlayerReb.attributes.reboundAttributes._distanceToReboundLocation = DistanceCalculator.getDistance2D(singlePlayerReb.getPosition(), thisRebound._rebFrame.ballAtThisFrame.getBallPosition())
                singlePlayerShot.attributes.reboundAttributes._distanceToReboundLocation = DistanceCalculator.getDistance2D(singlePlayerShot.getPosition(), thisRebound._rebFrame.ballAtThisFrame.getBallPosition())
                singlePlayerShot.attributes.reboundAttributes._angleFromReboundLocation = AngleCalculator.getAngleBetweenTwoPoints(singlePlayerShot.getPosition(), thisRebound._rebFrame.ballAtThisFrame.getBallPosition())
                singlePlayerReb.attributes.reboundAttributes._angleFromReboundLocation = AngleCalculator.getAngleBetweenTwoPoints(singlePlayerReb.getPosition(), thisRebound._rebFrame.ballAtThisFrame.getBallPosition())
                singlePlayerReb.attributes.reboundAttributes._changeInXDuringRebound = abs(singlePlayerShot.x) - abs(singlePlayerReb.x)

                if singlePlayerReb.attributes.distanceFromBasket < MAX_DISTANCE_TO_BE_REBOUNDING and singlePlayerShot.attributes.distanceFromBasket > MIN_DISTANCE_TO_BE_CRASHING:
                    
                    singlePlayerReb.attributes.reboundAttributes._crashedForRebound = True
                    
                    if singlePlayerReb.teamId == thisGameInfo._homeTeamID:
                        thisRebound._attributes._numberOfHomePlayersCrashing = thisRebound._attributes._numberOfHomePlayersCrashing + 1
                    else:
                        thisRebound._attributes._numberOfAwayPlayersCrashing = thisRebound._attributes._numberOfAwayPlayersCrashing + 1

                if singlePlayerReb.attributes.reboundAttributes._changeInXDuringRebound > MIN_X_DISTANCE_TO_LEAK_OUT and abs(singlePlayerReb.x) < MIN_X_LOCATION_TO_LEAK_OUT:

                    singlePlayerReb.attributes.reboundAttributes._inTransitionDuringRebound = True

                    if singlePlayerReb.teamId == thisGameInfo._homeTeamID:
                        thisRebound._attributes._numberOfHomePlayersInTransition = thisRebound._attributes._numberOfHomePlayersInTransition + 1
                    else:
                        thisRebound._attributes._numberOfAwayPlayersInTransition = thisRebound._attributes._numberOfAwayPlayersInTransition + 1

                break

    if thisRebound._shotEvent.teamId == thisGameInfo._homeTeamID:
        thisRebound._attributes._numberOfDefPlayersLeakingOut = thisRebound._attributes._numberOfAwayPlayersInTransition
        thisRebound._attributes._numberOfOffPlayersGettingBack = thisRebound._attributes._numberOfHomePlayersInTransition
    else:
        thisRebound._attributes._numberOfDefPlayersLeakingOut = thisRebound._attributes._numberOfHomePlayersInTransition
        thisRebound._attributes._numberOfOffPlayersGettingBack = thisRebound._attributes._numberOfAwayPlayersInTransition

    if thisRebound._rebEvent.teamId == thisRebound._shotEvent.teamId:
        if thisRebound._rebEvent.teamId == thisGameInfo._homeTeamID:
            thisRebound._attributes._numberOfOffPlayersCrashing = thisRebound._attributes._numberOfHomePlayersCrashing
            thisRebound._attributes._numberOfDefPlayersCrashing = thisRebound._attributes._numberOfAwayPlayersCrashing
        else:
            thisRebound._attributes._numberOfOffPlayersCrashing = thisRebound._attributes._numberOfAwayPlayersCrashing
            thisRebound._attributes._numberOfDefPlayersCrashing = thisRebound._attributes._numberOfHomePlayersCrashing
    else:
        if thisRebound._rebEvent.teamId == thisGameInfo._homeTeamID:
            thisRebound._attributes._numberOfOffPlayersCrashing = thisRebound._attributes._numberOfAwayPlayersCrashing
            thisRebound._attributes._numberOfDefPlayersCrashing = thisRebound._attributes._numberOfHomePlayersCrashing
        else:
            thisRebound._attributes._numberOfOffPlayersCrashing = thisRebound._attributes._numberOfHomePlayersCrashing
            thisRebound._attributes._numberOfDefPlayersCrashing = thisRebound._attributes._numberOfAwayPlayersCrashing

def CalculateReboundMetrics(shotReboundPairs):
 

    for singleReboundData in shotReboundPairs:
        singleReboundData._attributes._listPlayersFromBasketatShot = CalculateListofPlayersSortedByDistanceToBasket(singleReboundData._shotFrame)
        singleReboundData._attributes._listPlayersFromBasketatReb = CalculateListofPlayersSortedByDistanceToBasket(singleReboundData._rebFrame)
        CountNumberOfPlayersRebounding(singleReboundData)
        CountPlayersCrashingForRebound(singleReboundData)
        singleReboundData._attributes._listPlayersFromRebLoc = CalculateListofPlayersSortedByDistanceToRebLoc(singleReboundData)
        CalculateIfReboundInOpponentArea(singleReboundData)
        CountCrossedOpponents(singleReboundData)
        x=2

    
    
def WriteReboundMetricsToCSV(shotReboundPairs, playerList):
    fileNameForOutput = "C:\TrackingData\MiamiGame\TestCSVRebounds.csv"
    f = open(fileNameForOutput, 'w', newline='') 
    writer = csv.writer(f)

    fileNameForOutputRebPlayers = "C:\TrackingData\MiamiGame\TestCSVReboundsPlayers.csv"
    fRebPlayer = open(fileNameForOutputRebPlayers, 'w', newline='') 
    writerRebPlayer = csv.writer(fRebPlayer)

    for rebound in shotReboundPairs:

        rebounderName = FindPlayerLastNameByID(rebound._rebEvent.playerId, playerList)
        reboundingTeamId = rebound._rebEvent.teamId

        writer.writerow([rebound._rebEvent.eventNum, 
                         rebound._rebEvent.gameClock, 
                         rebounderName,
                         rebound._attributes._wasReboundedInOppArea,
                         rebound._attributes._numberOfHomePlayersTryingForReb, 
                         rebound._attributes._numberOfAwayPlayersTryingForReb,
                         rebound._attributes._numberOfOffPlayersTryingForReb,
                         rebound._attributes._numberOfDefPlayersTryingForReb,
                         rebound._attributes._numberOfHomePlayersCrashing,
                         rebound._attributes._numberOfAwayPlayersCrashing,
                         rebound._attributes._numberOfOffPlayersCrashing,
                         rebound._attributes._numberOfDefPlayersCrashing,
                         rebound._attributes._numberOfHomePlayersInTransition,
                         rebound._attributes._numberOfAwayPlayersInTransition,
                         rebound._attributes._numberOfDefPlayersLeakingOut,
                         rebound._attributes._numberOfOffPlayersGettingBack])

        for player in rebound._attributes._listPlayersFromBasketatReb:

            for shotPlayer in rebound._attributes._listPlayersFromBasketatShot:
                
                if player.playerId == shotPlayer.playerId: 
                    samePlayerAtShot = shotPlayer
                    break


            didIGetRebound = False

            if player.playerId == rebound._rebEvent.playerId: didIGetRebound = True
            
            playerName = FindPlayerLastNameByID(player.playerId, playerList)

            writerRebPlayer.writerow([rebound._rebEvent.eventNum, 
                                      rebound._rebEvent.gameClock,
                                      playerName,
                                      didIGetRebound,
                                      player.attributes.reboundAttributes._gotReboundinOpponentArea,
                                      player.attributes.reboundAttributes._triedForRebound,
                                      player.attributes.reboundAttributes._crashedForRebound,
                                      player.attributes.reboundAttributes._inTransitionDuringRebound,
                                      player.attributes.reboundAttributes._changeInSquareFootageControlled,
                                      player.attributes.reboundAttributes._distanceTravelledToGetRebound,
                                      samePlayerAtShot.attributes.reboundAttributes._distanceToReboundLocation,
                                      samePlayerAtShot.attributes.reboundAttributes._numberOfCloserOpponents])

def DoBoxOutWork(thisGame, fileNameCSVOutput):

    start_time = time.time()

    myLoader = JSON_Loader.JSON_Loader() 
    fileNamePlayers = "C:\TrackingData\MiamiGame\players.json" 
    myPlayerDictionary = myLoader.LoadJSON(fileNamePlayers)['players']

    allShotReboundPairs = []

    myBoxOutCandidateList = []

    allShotReboundPairs = FindReboundCandidates(thisGame, myBoxOutCandidateList)

    CalculateReboundMetrics(allShotReboundPairs)

    WriteReboundMetricsToCSV(allShotReboundPairs, myPlayerDictionary)

    MarkShootersAndRebounders(myBoxOutCandidateList)

    CleanBoxOutCandidates(myBoxOutCandidateList)

    myBoxOutCandidateList = RemoveNonCandidates(myBoxOutCandidateList)

    
    print("Game Complete")
    
    
    
    db = DBProvider('CoreStats')
    cursor = db.ExecuteQuery("Select Event_Num from dbo.Game_Event where game_id = '" + thisGameInfo._gameIDNBA + "'  and Event_Msg_Type = 3", 'DRIVER')

    listOfAllFTs = []

    for row in cursor.fetchall():
        listOfAllFTs.append(row[0])

    cursor = db.ExecuteQuery("Select Event_Num from dbo.Game_Event where game_id = '" + thisGameInfo._gameIDNBA + "'  and Event_Msg_Type = 1", 'DRIVER')

    listOfAllMadeFGs = []

    for row in cursor.fetchall():
        listOfAllMadeFGs.append(row[0])

    hustle = HustleStatRetrieval('CoreStats')
    hustleTypeEvents = hustle.GetHustleStatForGameByType(thisGameInfo._gameIDNBA, "BOX_OUT")

    #List of candidates to mark as good or in need of checking
    

    #for each hustlestat from the database, link candidates by event number, if multiple matches check, else good (may need optimizing)
    for h in hustleTypeEvents:
       hasBeenMatched = False
       matchedCandidateToHustleList = []
       h.ssId = NbaPlayerIdSSMap(h.personId, myPlayerDictionary)
       for candidate in myBoxOutCandidateList:
           if candidate._shotFrameEventNum == h.eventNum and (candidate._homePlayer.playerId == h.ssId or candidate._awayPlayer.playerId == h.ssId):
               matchedCandidateToHustleList.append(candidate)
               hasBeenMatched = True

       if len(matchedCandidateToHustleList) > 1:
           for matchedCandidate in matchedCandidateToHustleList:
               matchedCandidate._isLoggedAsBoxOut = -1
       
       else:
           for matchedCandidate in matchedCandidateToHustleList:
               matchedCandidate._isLoggedAsBoxOut = 1

       if not hasBeenMatched:
           if h.eventNum not in listOfAllFTs:
               print("*****BOX OUT MISSED AT EVENT NUM: " + str(h.eventNum))


    for singleCandidate in myBoxOutCandidateList:
        if singleCandidate._shotFrameEventNum in listOfAllMadeFGs:
            singleCandidate._isMadeFGA = 1
    #fileNameForOutput = "C:\TrackingData\MiamiGame\TestCSV.csv"
    WriteCandidatestoCSV(myBoxOutCandidateList, myPlayerDictionary, fileNameCSVOutput)
    print("--- %s seconds ---" % (time.time() - start_time))

def CalculateReboundsFromPickle():
    
    start_time = time.time()
    myPeriodList = []
    myBoxOutCandidateList = []

    directoryForPickleSaves = "C:\TrackingData\PickleSaves"
    pickleFiles = next(os.walk(directoryForPickleSaves))[2]
    for singleFile in pickleFiles:
        fullFilePath = os.path.join(directoryForPickleSaves, singleFile)
        with open (fullFilePath, 'rb') as filetoread:
            myPeriodList.append(pickle.load(filetoread))
    pickleLoadedGame = Game(0, myPeriodList)
    
    fileNameForOutput = "C:\TrackingData\TestCSV3.csv"
    DoBoxOutWork(pickleLoadedGame, fileNameForOutput)

    
def MarkShootersAndRebounders(candidateList):
    
    for candidate in candidateList:
        for event in candidate._possession.events:
            if event.eventType == 'SHOT':
                if candidate._homePlayer.playerId == event.playerId or candidate._awayPlayer.playerId == event.playerId:
                    candidate._shooterInovlved = 1
            elif event.eventType == "REB":
                if candidate._homePlayer.playerId == event.playerId or candidate._awayPlayer.playerId == event.playerId:
                    candidate._rebounderInvolved = 1

def CleanBoxOutCandidates(candidateList):
    
    for singleBoxOutToCheck in candidateList:
        allCandidatesForHomePlayer = []
        allCandidatesForAwayPlayer = []
        thisEventNumber = singleBoxOutToCheck._shotFrameEventNum
        for additionalBoxOut in candidateList:
            if additionalBoxOut._shotFrameEventNum == thisEventNumber:

                if (additionalBoxOut._homePlayer.playerId == singleBoxOutToCheck._homePlayer.playerId  
                    and not additionalBoxOut._awayPlayer.playerId == singleBoxOutToCheck._awayPlayer.playerId):
                
                    allCandidatesForHomePlayer.append(additionalBoxOut)

                if (additionalBoxOut._awayPlayer.playerId == singleBoxOutToCheck._awayPlayer.playerId
                    and not additionalBoxOut._homePlayer.playerId == singleBoxOutToCheck._homePlayer.playerId):

                    allCandidatesForAwayPlayer.append(additionalBoxOut)


        if len(allCandidatesForHomePlayer) > 0:
            for boxOutToCheckForDistance in allCandidatesForHomePlayer:
                if ((boxOutToCheckForDistance._attributes.totalDistanceBetweenPlayers / boxOutToCheckForDistance._attributes.wrestlingTime) < (singleBoxOutToCheck._attributes.totalDistanceBetweenPlayers / singleBoxOutToCheck._attributes.wrestlingTime)):
                    singleBoxOutToCheck._toBeKeptInList = False


        if len(allCandidatesForAwayPlayer) > 0:
            for boxOutToCheckForDistance in allCandidatesForAwayPlayer:
                if ((boxOutToCheckForDistance._attributes.totalDistanceBetweenPlayers / boxOutToCheckForDistance._attributes.wrestlingTime) < (singleBoxOutToCheck._attributes.totalDistanceBetweenPlayers / singleBoxOutToCheck._attributes.wrestlingTime)):
                    singleBoxOutToCheck._toBeKeptInList = False


def WriteCandidatestoCSV(candidateList, playerList, filename):
    f = open(filename, 'w', newline='') 
    try:
        writer = csv.writer(f)
        
        for candidate in candidateList:
            
            if candidate._toBeKeptInList == True:
            
                candidateFeatureList = []

                candidateFeatureList.append(candidate._shooterInovlved)
                candidateFeatureList.append(candidate._rebounderInvolved)

                try: #This is where we calculate all features from data stored in candidates and output to CSV
            
                    homeLastName = FindPlayerLastNameByID(candidate._homePlayer.playerId, playerList)
                    awayLastName = FindPlayerLastNameByID(candidate._awayPlayer.playerId, playerList)

                    homePlayerShot = FindPlayerFromTeam(candidate._homePlayer, candidate._shotFrame.teamHome.playersOnTeam)
                    awayPlayerShot = FindPlayerFromTeam(candidate._awayPlayer, candidate._shotFrame.teamAway.playersOnTeam)

                    homePlayerStart = FindPlayerFromTeam(candidate._homePlayer, candidate._startFrame.teamHome.playersOnTeam)
                    awayPlayerStart = FindPlayerFromTeam(candidate._awayPlayer, candidate._startFrame.teamAway.playersOnTeam)

                    homePlayerEnd = FindPlayerFromTeam(candidate._homePlayer, candidate._endFrame.teamHome.playersOnTeam)
                    awayPlayerEnd = FindPlayerFromTeam(candidate._awayPlayer, candidate._endFrame.teamAway.playersOnTeam)

                    homePlayerCollision = FindPlayerFromTeam(candidate._homePlayer, candidate._attributes.collisionFrame.teamHome.playersOnTeam)
                    awayPlayerCollision = FindPlayerFromTeam(candidate._awayPlayer, candidate._attributes.collisionFrame.teamAway.playersOnTeam)

                
                    averageDistFromBasketAtStart = (homePlayerStart.attributes.distanceFromBasket + awayPlayerStart.attributes.distanceFromBasket) / 2
                    candidateFeatureList.append(averageDistFromBasketAtStart)

                    homePlayerDistanceFromBasketDiff = homePlayerStart.attributes.distanceFromBasket - homePlayerEnd.attributes.distanceFromBasket
                    awayPlayerDistanceFromBasketDiff = awayPlayerStart.attributes.distanceFromBasket - awayPlayerEnd.attributes.distanceFromBasket

                    candidateDistanceFromBasketDiff = abs(homePlayerDistanceFromBasketDiff - awayPlayerDistanceFromBasketDiff)
                    candidateFeatureList.append(candidateDistanceFromBasketDiff)
                    candidateFeatureList.append(candidate._attributes.wrestlingTime)

                    averagePlayerDistanceFromEachOther = candidate._attributes.totalDistanceBetweenPlayers / candidate._attributes.wrestlingTime
                    candidateFeatureList.append(averagePlayerDistanceFromEachOther)
                    candidateFeatureList.append(candidate._attributes.minimumDistanceBetweenPlayers)

                    homePlayerAverageSpeed = candidate._attributes.homePlayerTotalAccumulatedSpeedDuringWrestling / candidate._attributes.wrestlingTime
                    awayPlayerAverageSpeed = candidate._attributes.awayPlayerTotalAccumulatedSpeedDuringWrestling / candidate._attributes.wrestlingTime
                
                    playerAverageSpeedDiff = abs(homePlayerAverageSpeed - awayPlayerAverageSpeed)
                    candidateFeatureList.append(playerAverageSpeedDiff)

                    playerCollisionSpeedDiff = abs(homePlayerCollision.attributes.speed - awayPlayerCollision.attributes.speed)
                    candidateFeatureList.append(playerCollisionSpeedDiff)

                    homePlayerAccelerationDiff = abs(candidate._attributes.homePlayerMaxAcceleration - homePlayerCollision.attributes.acceleration)
                    candidateFeatureList.append(homePlayerAccelerationDiff)

                    awayPlayerAccelerationDiff = abs(candidate._attributes.awayPlayerMaxAcceleration - awayPlayerCollision.attributes.acceleration)
                    candidateFeatureList.append(awayPlayerAccelerationDiff)

                    homePlayerMaxSpeedDiff = abs(candidate._attributes.homePlayerMaximumSpeed - homePlayerCollision.attributes.speed)
                    candidateFeatureList.append(homePlayerMaxSpeedDiff)

                    awayPlayerMaxSpeedDiff = abs(candidate._attributes.awayPlayerMaximumSpeed - awayPlayerCollision.attributes.speed)
                    candidateFeatureList.append(awayPlayerMaxSpeedDiff)
                

                

                    candidateDistanceVelocityScore = ((homePlayerAverageSpeed + awayPlayerAverageSpeed)*1000) / max(averagePlayerDistanceFromEachOther, 1)

                    x = np.array(candidate._attributes.distanceBetweenPlayersList)
                    y = savgol_filter(x, 5, 2, mode='nearest')
                    ydiff = np.diff(y)

                    totalFramesInVeryCloseProximity = 0
                    totalFramesWithDerivateNearZero = 0

                    for index, ySingle in enumerate(y):
                    
                        if ySingle < 3: totalFramesInVeryCloseProximity = totalFramesInVeryCloseProximity + 1

                        if index > 0: 
                            if abs(ydiff[index-1]) < 0.1: totalFramesWithDerivateNearZero = totalFramesWithDerivateNearZero + 1
                
                    ratioOfFramesWithLowDerivativeToTotal = totalFramesWithDerivateNearZero / y.size

                    candidateFeatureList.append(totalFramesInVeryCloseProximity)
                    candidateFeatureList.append(ratioOfFramesWithLowDerivativeToTotal)
                    candidateFeatureList.append(candidateDistanceVelocityScore)

                    #toBeClassified = np.array([candidateFeatureList]).astype('float')

                    #prediction = singlemodel.predict(toBeClassified)

                    #prediction_prob = singlemodel.predict_proba(toBeClassified)

                    #prediction_prob_one = oneModel.predict(toBeClassified)

                    #prediction_prob_iso = isoForest.predict(toBeClassified)

                    #listOfLists.append(candidateFeatureList)
            
                except Exception as e: #Presently lame catch all to make it obvious if a candidate fails.
                    print(e)
                    averageDistFromBasketAtStart = -11
                    candidateDistanceFromBasketDiff = -11
                    playerAverageSpeedDiff = -11
                    playerCollisionSpeedDiff = -11
                    homePlayerAccelerationDiff = -11
                    awayPlayerAccelerationDiff = -11
                    homePlayerMaxSpeedDiff = -11
                    awayPlayerMaxSpeedDiff  = -11
                    totalFramesInVeryCloseProximity = -11
                    totalFramesWithDerivateNearZero = -11
                    ratioOfFramesWithLowDerivativeToTotal = -11



                if averageDistFromBasketAtStart < 20 and totalFramesInVeryCloseProximity > 10:
                    writer.writerow([candidate._isLoggedAsBoxOut,
                                     homeLastName, 
                                     awayLastName,
                                     thisGameInfo._gameIDNBA,
                                     candidate._shotFrameEventNum,
                                     candidate._shotFrame.period, 
                                     candidate._shotFrame.gameClock,
                                     candidate._isMadeFGA,
                                     #candidate._shotFrame.frameID, 
                                     #candidate._startFrame.frameID,
                                     #candidate._endFrame.frameID,
                                     candidate._shooterInovlved,
                                     candidate._rebounderInvolved,
                                     averageDistFromBasketAtStart,  
                                     candidateDistanceFromBasketDiff,
                                     candidate._attributes.wrestlingTime,
                                     averagePlayerDistanceFromEachOther,
                                     candidate._attributes.minimumDistanceBetweenPlayers,
                                     playerAverageSpeedDiff,
                                     playerCollisionSpeedDiff,
                                     homePlayerAccelerationDiff,
                                     awayPlayerAccelerationDiff,
                                     homePlayerMaxSpeedDiff,
                                     awayPlayerMaxSpeedDiff,
                                     totalFramesInVeryCloseProximity,
                                     ratioOfFramesWithLowDerivativeToTotal,
                                     candidateDistanceVelocityScore])
                elif candidate._isLoggedAsBoxOut != 0: print("CANDIDATE EXCLUDED! Event Num: " + str(candidate._shotFrameEventNum) + " " + homeLastName + " " + awayLastName + " Distance from Basket: " + str(averageDistFromBasketAtStart) + " Close Proximity Frames: " + str(totalFramesInVeryCloseProximity))
    finally:
        f.close()


def WriteCandidatestoCSVwithML(candidateList, playerList, filename):
    
    listOfLists = []
    prediction = -2
    filenameForTrainingData = 'C:\TrackingData\SmallTrainingSetv3.csv'
    raw_data = open(filenameForTrainingData, 'rt')
    reader = _csv.reader(raw_data, delimiter=',', quoting=_csv.QUOTE_NONE)
    #numpyData = np.loadtxt(raw_data, delimiter=',')
    #headers = next(reader)
    allDataAsList = list(reader)
    allDataAsList.pop(0)
    
        #Remove header of CSV with column names

    data = np.array(allDataAsList).astype('float')
    X = data[:, 4:]
    y = data[:, 0]
    

    C = 1.0
    
    singlemodel = svm.SVC(kernel = 'rbf', probability = True)
    singlemodel.fit(X,y)

    oneModel = svm.OneClassSVM(nu=0.1, kernel='rbf', gamma=0.1)
    oneModel.fit(X,y)

    isoForest = IsolationForest(max_samples=5000)
    isoForest.fit(X,y)

    fileNameForModel = 'C:\TrackingData\PickleModel'
    with open(fileNameForModel, 'wb') as filetowrite:
            pickle.dump(singlemodel, filetowrite)

    f = open(filename, 'w', newline='') 
    try:
        writer = csv.writer(f)
        
        for candidate in candidateList:
            
            if candidate._toBeKeptInList == True:
            
                candidateFeatureList = []

                candidateFeatureList.append(candidate._shooterInovlved)
                candidateFeatureList.append(candidate._rebounderInvolved)

                try: #This is where we calculate all features from data stored in candidates and output to CSV
            
                    homeLastName = FindPlayerLastNameByID(candidate._homePlayer.playerId, playerList)
                    awayLastName = FindPlayerLastNameByID(candidate._awayPlayer.playerId, playerList)

                    homePlayerShot = FindPlayerFromTeam(candidate._homePlayer, candidate._shotFrame.teamHome.playersOnTeam)
                    awayPlayerShot = FindPlayerFromTeam(candidate._awayPlayer, candidate._shotFrame.teamAway.playersOnTeam)

                    homePlayerStart = FindPlayerFromTeam(candidate._homePlayer, candidate._startFrame.teamHome.playersOnTeam)
                    awayPlayerStart = FindPlayerFromTeam(candidate._awayPlayer, candidate._startFrame.teamAway.playersOnTeam)

                    homePlayerEnd = FindPlayerFromTeam(candidate._homePlayer, candidate._endFrame.teamHome.playersOnTeam)
                    awayPlayerEnd = FindPlayerFromTeam(candidate._awayPlayer, candidate._endFrame.teamAway.playersOnTeam)

                    homePlayerCollision = FindPlayerFromTeam(candidate._homePlayer, candidate._attributes.collisionFrame.teamHome.playersOnTeam)
                    awayPlayerCollision = FindPlayerFromTeam(candidate._awayPlayer, candidate._attributes.collisionFrame.teamAway.playersOnTeam)

                
                    averageDistFromBasketAtStart = (homePlayerStart.attributes.distanceFromBasket + awayPlayerStart.attributes.distanceFromBasket) / 2
                    candidateFeatureList.append(averageDistFromBasketAtStart)

                    homePlayerDistanceFromBasketDiff = homePlayerStart.attributes.distanceFromBasket - homePlayerEnd.attributes.distanceFromBasket
                    awayPlayerDistanceFromBasketDiff = awayPlayerStart.attributes.distanceFromBasket - awayPlayerEnd.attributes.distanceFromBasket

                    candidateDistanceFromBasketDiff = abs(homePlayerDistanceFromBasketDiff - awayPlayerDistanceFromBasketDiff)
                    candidateFeatureList.append(candidateDistanceFromBasketDiff)
                    candidateFeatureList.append(candidate._attributes.wrestlingTime)

                    averagePlayerDistanceFromEachOther = candidate._attributes.totalDistanceBetweenPlayers / candidate._attributes.wrestlingTime
                    candidateFeatureList.append(averagePlayerDistanceFromEachOther)
                    candidateFeatureList.append(candidate._attributes.minimumDistanceBetweenPlayers)

                    homePlayerAverageSpeed = candidate._attributes.homePlayerTotalAccumulatedSpeedDuringWrestling / candidate._attributes.wrestlingTime
                    awayPlayerAverageSpeed = candidate._attributes.awayPlayerTotalAccumulatedSpeedDuringWrestling / candidate._attributes.wrestlingTime
                
                    playerAverageSpeedDiff = abs(homePlayerAverageSpeed - awayPlayerAverageSpeed)
                    candidateFeatureList.append(playerAverageSpeedDiff)

                    playerCollisionSpeedDiff = abs(homePlayerCollision.attributes.speed - awayPlayerCollision.attributes.speed)
                    candidateFeatureList.append(playerCollisionSpeedDiff)

                    homePlayerAccelerationDiff = abs(candidate._attributes.homePlayerMaxAcceleration - homePlayerCollision.attributes.acceleration)
                    candidateFeatureList.append(homePlayerAccelerationDiff)

                    awayPlayerAccelerationDiff = abs(candidate._attributes.awayPlayerMaxAcceleration - awayPlayerCollision.attributes.acceleration)
                    candidateFeatureList.append(awayPlayerAccelerationDiff)

                    homePlayerMaxSpeedDiff = abs(candidate._attributes.homePlayerMaximumSpeed - homePlayerCollision.attributes.speed)
                    candidateFeatureList.append(homePlayerMaxSpeedDiff)

                    awayPlayerMaxSpeedDiff = abs(candidate._attributes.awayPlayerMaximumSpeed - awayPlayerCollision.attributes.speed)
                    candidateFeatureList.append(awayPlayerMaxSpeedDiff)
                

                

                    candidateDistanceVelocityScore = ((homePlayerAverageSpeed + awayPlayerAverageSpeed)*1000) / max(averagePlayerDistanceFromEachOther, 1)

                    x = np.array(candidate._attributes.distanceBetweenPlayersList)
                    y = savgol_filter(x, 5, 2, mode='nearest')
                    ydiff = np.diff(y)

                    totalFramesInVeryCloseProximity = 0
                    totalFramesWithDerivateNearZero = 0

                    for index, ySingle in enumerate(y):
                    
                        if ySingle < 2.5: totalFramesInVeryCloseProximity = totalFramesInVeryCloseProximity + 1

                        if index > 0: 
                            if abs(ydiff[index-1]) < 0.1: totalFramesWithDerivateNearZero = totalFramesWithDerivateNearZero + 1
                
                    ratioOfFramesWithLowDerivativeToTotal = totalFramesWithDerivateNearZero / y.size

                    candidateFeatureList.append(totalFramesInVeryCloseProximity)
                    candidateFeatureList.append(ratioOfFramesWithLowDerivativeToTotal)
                    candidateFeatureList.append(candidateDistanceVelocityScore)

                    toBeClassified = np.array([candidateFeatureList]).astype('float')

                    prediction = singlemodel.predict(toBeClassified)

                    prediction_prob = singlemodel.predict_proba(toBeClassified)

                    prediction_prob_one = oneModel.predict(toBeClassified)

                    prediction_prob_iso = isoForest.predict(toBeClassified)

                    listOfLists.append(candidateFeatureList)
            
                except Exception as e: #Presently lame catch all to make it obvious if a candidate fails.
                    print(e)
                    averageDistFromBasketAtStart = -11
                    candidateDistanceFromBasketDiff = -11
                    playerAverageSpeedDiff = -11
                    playerCollisionSpeedDiff = -11
                    homePlayerAccelerationDiff = -11
                    awayPlayerAccelerationDiff = -11
                    homePlayerMaxSpeedDiff = -11
                    awayPlayerMaxSpeedDiff  = -11
                    totalFramesInVeryCloseProximity = -11
                    totalFramesWithDerivateNearZero = -11
                    ratioOfFramesWithLowDerivativeToTotal = -11



                writer.writerow([candidate._isLoggedAsBoxOut,
                                 prediction_prob_iso[0],
                                 homeLastName, 
                                 awayLastName,
                                 candidate._shotFrameEventNum,
                                 candidate._shotFrame.period, 
                                 candidate._shotFrame.gameClock,
                                 #candidate._shotFrame.frameID, 
                                 #candidate._startFrame.frameID,
                                 #candidate._endFrame.frameID,
                                 candidate._shooterInovlved,
                                 candidate._rebounderInvolved,
                                 averageDistFromBasketAtStart,  
                                 candidateDistanceFromBasketDiff,
                                 candidate._attributes.wrestlingTime,
                                 averagePlayerDistanceFromEachOther,
                                 candidate._attributes.minimumDistanceBetweenPlayers,
                                 playerAverageSpeedDiff,
                                 playerCollisionSpeedDiff,
                                 homePlayerAccelerationDiff,
                                 awayPlayerAccelerationDiff,
                                 homePlayerMaxSpeedDiff,
                                 awayPlayerMaxSpeedDiff,
                                 totalFramesInVeryCloseProximity,
                                 ratioOfFramesWithLowDerivativeToTotal,
                                 candidateDistanceVelocityScore])
    finally:
        f.close()

def FindPlayerFromTeam(targetPlayer, team):
    for currentPlayer in team:
        if targetPlayer.playerId == currentPlayer.playerId: return currentPlayer

def FindReboundCandidates(myGame, boxOutCandidates):

    listOfShotRebFramePairs = []

    for currentPeriod in myGame.gamePeriods:
        for possIndex, currentPoss in enumerate(currentPeriod.possessionsForPeriod):#Loop through all possessions to look for ones with shots
            #if currentPoss.outcome == 'FGX' or currentPoss.outcome == 'FGM':
            if currentPoss.counts['FGA'] > 0:    
                trackingTimeForShotEvent = 0
                endTrackingTime = currentPoss.wallClockEnd

                if currentPoss.teamIdOffense == thisGameInfo._homeTeamID: #Figure out which basket you're shooting at
                    basketForThisPoss = myGame.gamePeriods[int(currentPoss.period)].basketTeamHome 
                elif currentPoss.teamIdOffense == thisGameInfo._awayTeamID:
                    basketForThisPoss = myGame.gamePeriods[int(currentPoss.period)].basketTeamAway
                else:
                    print("THERE WAS AN ERROR MATCHING HOME/AWAY TEAM IDS (and insert error handling code)")


                shotFrameForCalculations = Frame()
                rebFrameForCalculations = Frame()
                
                

                for eventIndex, currentEvent in enumerate(currentPoss.events): #Look for specific shot event within the possession
                    #1517447255847
                    if currentEvent.eventType == 'SHOT':
                        shotEvent = currentEvent
                        trackingTimeForShotEvent = currentEvent.timeStamp
                        eventNum = currentEvent.eventNum

                        wrestlingPlayers = {} #Initialize Dictionary to store potential candidates

                        if eventIndex < len(currentPoss.events)-1: #This if statement sets logic for the end of the window to search
                            try:
                                endTrackingTime = currentPoss.events[eventIndex+1].timeStamp
                            except:
                                print("Error Found")
                        elif possIndex < len(currentPeriod.possessionsForPeriod)-1:
                            try:
                                endTrackingTime = currentPeriod.possessionsForPeriod[possIndex+1].events[0].timeStamp
                                for nextPossTrackingFrame in currentPeriod.possessionsForPeriod[possIndex+1].trackingData:
                                    if nextPossTrackingFrame.timeStamp <= endTrackingTime:
                                        currentPoss.trackingData.append(nextPossTrackingFrame)
                                    else:
                                        break        
                            except:
                                print("Error Found")
                        else:
                            endTrackingTime = endTrackingTime + 3000

                        for frameIndex, currentFrame in enumerate(currentPoss.trackingData): #Now loop through all frames
                            if currentFrame.timeStamp == trackingTimeForShotEvent: 
                                shotFrameIndex = frameIndex #86083 404
                                shotFrame = currentFrame
                                shotFrame = CalculateReboundAreas(shotFrame) #KEY FUNCTION
                                shotFrame = WeightAreas(shotFrame, basketForThisPoss)
                                shotFrameForCalculations = shotFrame
                            
                           
                            if currentFrame.timeStamp > trackingTimeForShotEvent and currentFrame.timeStamp <= endTrackingTime: #Do business logic if frame is within possible shot window

                                DistanceCalculator.CalculatePlayerToOpponentAndBallDistanceSingleFrame(currentFrame, basketForThisPoss)

                                currentFrame = CalculateReboundAreas(currentFrame) #KEY FUNCTION
                                currentFrame = WeightAreas(currentFrame, basketForThisPoss)

                                for homePlayer in currentFrame.teamHome.playersOnTeam: #Search all player combos for their distance
                                    for playerIndex, playerDistance in enumerate(homePlayer.attributes.distanceFromOpponents):
                                        
                                        awayPlayer = currentFrame.teamAway.playersOnTeam[playerIndex]
                                        wrestlingKey = (homePlayer.playerId, awayPlayer.playerId)

                                        if playerDistance < MIN_DISTANCE_BETWEEN_PLAYERS: #If distance less than MIN_DISTANCE_BETWEEN_PLAYERS feet, this could be a candidate
                                            if wrestlingKey not in wrestlingPlayers:
                                                wrestlingPlayers[wrestlingKey] = BoxOutCandidateAttributes(playerDistance, 0, 0, 1, currentFrame)
                                                currentFrame = CalculateReboundAreas(currentFrame) #KEY FUNCTION
                                                currentFrame = WeightAreas(currentFrame, basketForThisPoss)

                                                wrestlingPlayers[wrestlingKey].distanceBetweenPlayersList.append(playerDistance)

                                            else: #If possible candidate already exists, increment attributes
                                                wrestlingPlayers[wrestlingKey].wrestlingTime = wrestlingPlayers[wrestlingKey].wrestlingTime + 1
                                                wrestlingPlayers[wrestlingKey].totalDistanceBetweenPlayers = wrestlingPlayers[wrestlingKey].totalDistanceBetweenPlayers + playerDistance
                                                wrestlingPlayers[wrestlingKey].homePlayerTotalAccumulatedSpeedDuringWrestling = wrestlingPlayers[wrestlingKey].homePlayerTotalAccumulatedSpeedDuringWrestling + homePlayer.attributes.speed
                                                wrestlingPlayers[wrestlingKey].awayPlayerTotalAccumulatedSpeedDuringWrestling = wrestlingPlayers[wrestlingKey].awayPlayerTotalAccumulatedSpeedDuringWrestling + awayPlayer.attributes.speed

                                                if wrestlingPlayers[wrestlingKey].minimumDistanceBetweenPlayers > playerDistance:
                                                    wrestlingPlayers[wrestlingKey].minimumDistanceBetweenPlayers = playerDistance
                                                    wrestlingPlayers[wrestlingKey].collisionFrame = currentFrame

                                                if wrestlingPlayers[wrestlingKey].homePlayerMaxAcceleration < homePlayer.attributes.acceleration:
                                                    wrestlingPlayers[wrestlingKey].homePlayerMaxAcceleration = homePlayer.attributes.acceleration

                                                if wrestlingPlayers[wrestlingKey].awayPlayerMaxAcceleration < awayPlayer.attributes.acceleration:
                                                    wrestlingPlayers[wrestlingKey].awayPlayerMaxAcceleration = awayPlayer.attributes.acceleration

                                                if wrestlingPlayers[wrestlingKey].homePlayerMaximumSpeed < homePlayer.attributes.speed:
                                                    wrestlingPlayers[wrestlingKey].homePlayerMaximumSpeed = homePlayer.attributes.speed

                                                if wrestlingPlayers[wrestlingKey].awayPlayerMaximumSpeed < awayPlayer.attributes.speed:
                                                    wrestlingPlayers[wrestlingKey].awayPlayerMaximumSpeed = awayPlayer.attributes.speed

                                                wrestlingPlayers[wrestlingKey].distanceBetweenPlayersList.append(playerDistance)

                                        else:
                                            if wrestlingKey in wrestlingPlayers: #Time to remove a candidate and check to see if the players were near each other for long enough
                                                value = wrestlingPlayers.pop(wrestlingKey, None)
                                                if value.wrestlingTime > NUMBER_OF_FRAMES_WRESTLING_TO_QUALIFY_AS_CANDIDATE: #If players were near each other for long enough, then we have an actual Box Out Candidate
                                                    #candidateFrames[wrestlingKey] = (currentPoss.trackingData[frameIndex-value], currentFrame)
                                                    currentFrame = CalculateReboundAreas(currentFrame) #KEY FUNCTION
                                                    currentFrame = WeightAreas(currentFrame, basketForThisPoss)

                                                    boxOutCandidates.append(BoxOutCandidate(homePlayer, awayPlayer, currentPoss.trackingData[frameIndex-value.wrestlingTime], currentFrame, shotFrame, currentPoss, eventNum, value))

                            elif currentFrame.timeStamp > endTrackingTime:
                            #if currentFrame.timeStamp == endTrackingTime:
                                previouslyVisitedKeys = []
                                for key in wrestlingPlayers: #Catch all logic in case we get to the end of a window with potential candidates still in the dictionary

                                    if key not in previouslyVisitedKeys:

                                        if wrestlingPlayers[key].wrestlingTime > NUMBER_OF_FRAMES_WRESTLING_TO_QUALIFY_AS_CANDIDATE:

                                            for thisHomePlayer in currentFrame.teamHome.playersOnTeam:
                                                if thisHomePlayer.playerId == key[0]: homePlayerCandidate = thisHomePlayer

                                            for thisAwayPlayer in currentFrame.teamAway.playersOnTeam:
                                                if thisAwayPlayer.playerId == key[1]: awayPlayerCandidate = thisAwayPlayer

                                            currentFrame = CalculateReboundAreas(currentFrame) #KEY FUNCTION
                                            currentFrame = WeightAreas(currentFrame, basketForThisPoss)
                                            boxOutCandidates.append(BoxOutCandidate(homePlayerCandidate, awayPlayerCandidate, currentPoss.trackingData[frameIndex - wrestlingPlayers[key].wrestlingTime], currentFrame, shotFrame, currentPoss, eventNum, wrestlingPlayers[key]))
                                    else:
                                        previouslyVisitedKeys.append(key)
                                break
                                       


                                                    
                        

                        else:
                        #if currentFrame.timeStamp == endTrackingTime:
                            previouslyVisitedKeys = []
                            for key in wrestlingPlayers: #Catch all logic in case we get to the end of a window with potential candidates still in the dictionary

                                if key not in previouslyVisitedKeys:

                                    if wrestlingPlayers[key].wrestlingTime > NUMBER_OF_FRAMES_WRESTLING_TO_QUALIFY_AS_CANDIDATE:

                                        for thisHomePlayer in currentFrame.teamHome.playersOnTeam:
                                            if thisHomePlayer.playerId == key[0]: homePlayerCandidate = thisHomePlayer

                                        for thisAwayPlayer in currentFrame.teamAway.playersOnTeam:
                                            if thisAwayPlayer.playerId == key[1]: awayPlayerCandidate = thisAwayPlayer

                                        currentFrame = CalculateReboundAreas(currentFrame) #KEY FUNCTION
                                        currentFrame = WeightAreas(currentFrame, basketForThisPoss)
                                        boxOutCandidates.append(BoxOutCandidate(homePlayerCandidate, awayPlayerCandidate, currentPoss.trackingData[frameIndex - wrestlingPlayers[key].wrestlingTime], currentFrame, shotFrame, currentPoss, eventNum, wrestlingPlayers[key]))
                                else:
                                    previouslyVisitedKeys.append(key)

                    elif currentEvent.eventType == 'REB' and currentEvent.location is not None:
                        trackingTimeForRebEvent = currentEvent.timeStamp
                        rebEvent = currentEvent
                        for frameIndex, currentFrame in enumerate(currentPoss.trackingData): #Now loop through all frames
                            if currentFrame.timeStamp == trackingTimeForRebEvent: 
                                rebFrameIndex = frameIndex #86083 404
                                rebFrame = currentFrame
                                rebFrame = CalculateReboundAreas(rebFrame) #KEY FUNCTION
                                rebFrame = WeightAreas(rebFrame, basketForThisPoss)
                                rebFrameForCalculations = rebFrame
                                #listOfShotRebFramePairs.append((shotFrameForCalculations,rebFrameForCalculations))
                                listOfShotRebFramePairs.append(ReboundData(shotEvent, rebEvent, shotFrameForCalculations, rebFrameForCalculations))
    
    return listOfShotRebFramePairs                            
                        

                                

def TestMLCode():
    #iris = datasets.load_iris()
    # Take the first two features. We could avoid this by using a two-dim dataset
    #X = iris.data[:, :2]
    #y = iris.target
    
    #csvData = []
    #targets = []
    
    #X = [[0, 0], [1, 1]]
    #y = [0, 2]
    #clf = svm.SVC(C=1.0, cache_size=200, class_weight=None, coef0=0.0,
    #decision_function_shape='ovr', degree=3, gamma='auto', kernel='rbf',
    #max_iter=-1, probability=True, random_state=None, shrinking=True,
    #tol=0.001, verbose=False)
    #clf.fit(X, y)  
    #svm.SVC(C=1.0, cache_size=200, class_weight=None, coef0=0.0,
    #decision_function_shape='ovr', degree=3, gamma='auto', kernel='rbf',
    #max_iter=-1, probability=True, random_state=None, shrinking=True,
    #tol=0.001, verbose=False)

    #print(clf.predict_proba([[1000., 10.]]))
     
    filename = 'C:\TrackingData\BoxOutSmallTrainingSetNoNames.csv'
    raw_data = open(filename, 'rt')
    reader = _csv.reader(raw_data, delimiter=',', quoting=_csv.QUOTE_NONE)
    #numpyData = np.loadtxt(raw_data, delimiter=',')
    #headers = next(reader)
    allDataAsList = list(reader)
    allDataAsList.pop(0)
    
     #Remove header of CSV with column names

    data = np.array(allDataAsList).astype('float')
    X = data[:, 4:]
    y = data[:, 0]
    

    C = 1.0
    
    singlemodel = svm.SVC(kernel = 'rbf', C=1, probability = True)
    scorer = make_scorer(roc_auc_score)

    grid_list = {"C": np.arange(2, 10, 2),
             "gamma": np.arange(0.1, 1, 0.2)}

    grid_search = GridSearchCV(singlemodel, param_grid = grid_list, n_jobs = 4, cv = 3, scoring = scorer) 
    grid_search.fit(X, y) 
    grid_search.cv_results_
    
    #scores = cross_val_score(singlemodel, X, y, cv=5)
    
    scores = cross_val_score(singlemodel, X, y, cv=3)

    singlemodel.fit(X,y)

    #pred = singlemodel.predict_proba([[100.,100.]])
    
    print(pred)
    
    

    filenamecounter = 1
    filenameforsave = "C:\TrackingData\Picklesave"

    with open (filenameforsave + str(filenamecounter), 'rb') as filetoread:
        secondmodel = pickle.load(filetoread)
    
    print(secondmodel.predict_proba([[100.,100.]]))
    
    with open(filenameforsave + str(filenamecounter), 'wb') as filetowrite:
        pickle.dump(singlemodel, filetowrite)
        
    print(singlemodel.predict_proba([[100.,100.]]))

   
        
        
    
    
    # we create an instance of SVM and fit out data. We do not scale our
    # data since we want to plot the support vectors
    #C = 1.0  # SVM regularization parameter
    #models = (svm.SVC(kernel='linear', C=C),
    #          svm.LinearSVC(C=C),
    #          svm.SVC(kernel='rbf', gamma=1.0, C=C),
    #          svm.SVC(kernel='poly', degree=6, C=C))
    #models = (svm.SVC(kernel='linear', C=C),
    #          svm.LinearSVC(C=C))
    #models = (clf.fit(X, y) for clf in models)
    
    
    
    # title for the plots
    #===========================================================================
    # titles = ('SVC with linear kernel',
    #           'LinearSVC (linear kernel)',
    #           'SVC with RBF kernel',
    #           'SVC with polynomial (degree 3) kernel')
    # 
    # # Set-up 2x2 grid for plotting.
    # fig, sub = plt.subplots(2, 2)
    # plt.subplots_adjust(wspace=0.4, hspace=0.4)
    # 
    # X0, X1 = X[:, 0], X[:, 1]
    # xx, yy = make_meshgrid(X0, X1)
    # 
    # for clf, title, ax in zip(models, titles, sub.flatten()):
    #     plot_contours(ax, clf, xx, yy,
    #                   cmap=plt.cm.coolwarm, alpha=0.8)
    #     ax.scatter(X0, X1, c=y, cmap=plt.cm.coolwarm, s=20, edgecolors='k')
    #     ax.set_xlim(xx.min(), xx.max())
    #     ax.set_ylim(yy.min(), yy.max())
    #     ax.set_xlabel('Sepal length')
    #     ax.set_ylabel('Sepal width')
    #     ax.set_xticks(())
    #     ax.set_yticks(())
    #     ax.set_title(title)
    # 
    # plt.show()
    #===========================================================================
    
    


    print("Hello World!")


 
def TestVoronoi(myGame):
    #This function is used for testing the new functions CalculateReboundAreas and WeightAreas.  Eventually this will need to be adapted when it's connected to our candidates

    testPossession = myGame.gamePeriods[1].possessionsForPeriod[4] #Hard Coded to a single possession for Testing.  Eventually loop through all candidates.
    playerLocations = []
    altPlayerLocations = []

    shotFound = False
    rebFound = False

    if testPossession.teamIdOffense == thisGameInfo._homeTeamID: #Figure out which basket you're shooting at
        basketForThisPoss = myGame.gamePeriods[int(testPossession.period)].basketTeamHome 
    elif testPossession.teamIdOffense == thisGameInfo._awayTeamID:
        basketForThisPoss = myGame.gamePeriods[int(testPossession.period)].basketTeamAway
    else:
        print("THERE WAS AN ERROR MATCHING HOME/AWAY TEAM IDS (and insert error handling code)")


    for thisEvent in testPossession.events:

        if thisEvent.eventType == 'SHOT':
            
            for thisFrame in testPossession.trackingData:

                if thisFrame.timeStamp == thisEvent.timeStamp:

                    shotFrame = CalculateReboundAreas(thisFrame) #KEY FUNCTION
                    shotFrame = WeightAreas(shotFrame, basketForThisPoss) #KEY FUNCTION
                    shotFound = True
 

                if shotFound == True and rebFound == False:
                    
                    distanceFromBallToBasket = math.sqrt(math.pow(thisFrame.ballAtThisFrame.x - basketForThisPoss.x, 2) + math.pow(thisFrame.ballAtThisFrame.y - basketForThisPoss.y, 2))
                    
                    if distanceFromBallToBasket < 3:
                        rebFrame = CalculateReboundAreas(thisFrame) #KEY FUNCTION
                        rebFrame = WeightAreas(rebFrame, basketForThisPoss) #KEY FUNCTION
                        rebFound = True

   
    





def CalculateReboundAreas(thisFrame):
    #Function uses Voronoi formula to figure out the area each player 'owns' at any given frame.
    #INPUT: Frame at which to figure out areas
    #OUTPUT: Same Frame with rebound area filled in for each player

    playerLocations = [] #Voronoi library requires list of player / point locations
    altPlayerLocations = [] #For reflected player locations

    for thisPlayer in thisFrame.teamHome.playersOnTeam: #Order in which points are appended is important to maintain so we can rejoin to the correct player later.  Home team always first before away team.
        location = [thisPlayer.x, thisPlayer.y]
                        
        playerLocations.append(location)
        altPlayerLocations.append(location)

    for thisPlayer in thisFrame.teamAway.playersOnTeam:
        location = [thisPlayer.x, thisPlayer.y]
                        
        playerLocations.append(location)
        altPlayerLocations.append(location)

    for singleLocation in playerLocations:
        altLocationXPos = [singleLocation[0] + 2*(52 - singleLocation[0]), singleLocation[1]] #These 4 lines reflect the player locations around each boundary of the court in order to bound the Voronoi areas to the court boundaries
        altLocationYPos = [singleLocation[0], singleLocation[1] + 2*(26 - singleLocation[1])] #Values larger than the actual court dimensions used to account for players running out of bounds
        altLocationXNeg = [singleLocation[0] + 2*(-52 - singleLocation[0]), singleLocation[1]]
        altLocationYNeg = [singleLocation[0], singleLocation[1] + 2*(-26 - singleLocation[1])]
                        
        altPlayerLocations.append(altLocationXPos)
        altPlayerLocations.append(altLocationXNeg)
        altPlayerLocations.append(altLocationYPos)
        altPlayerLocations.append(altLocationYNeg)

    finalArray = np.array(playerLocations)
    altFinalArray = np.array(altPlayerLocations)

    x, y = zip(*finalArray)

    vor = Voronoi(finalArray)
    #voronoi_plot_2d(vor)

    vor = Voronoi(altFinalArray)
    #voronoi_plot_2d(vor)


    listOfAreas = []

    for indexPoint, regionNum in enumerate(vor.point_region): #For each Voronoi region (Each player has a region)
        listOfVertices = []

        for indexVertex, vertexNum in enumerate(vor.regions[regionNum]): #Find all Voronoi verticies for that region
            listOfVertices.append(vor.vertices[vertexNum])

        a, b = zip(*listOfVertices)
        listOfAreas.append(0.5*np.abs(np.dot(a,np.roll(b,1))-np.dot(b,np.roll(a,1)))) #After finding all Voronoi vertices, use this implementation of Shoelace formula to calculate area of polygon
        

    #for region in vor.regions: #Sample code to color regions
    #    if not -1 in region:
    #        polygon = [vor.vertices[i] for i in region]
    #        plt.fill(*zip(*polygon))


    #fig, ax = plt.subplots()
    #ax.scatter(x, y)

    for i, txt in enumerate(listOfAreas): #Sample code to visually plot areas for testing
        teamHomeNumPlayers = len(thisFrame.teamHome.playersOnTeam)
        teamAwayNumPlayers = len(thisFrame.teamAway.playersOnTeam)
        if i < (teamAwayNumPlayers + teamAwayNumPlayers):
            #ax.annotate(txt, (x[i], y[i]))
            try:
                if i < teamHomeNumPlayers:
                    thisFrame.teamHome.playersOnTeam[i].reboundArea = txt #This code must be kept as it joins each area back to the player within the Frame for proper storage
                else:
                    thisFrame.teamAway.playersOnTeam[i-5].reboundArea = txt
            except:
                pass

    #if thisFrame.gameClock < 683:
    #    plt.show()

    #plt.close('all')
    return thisFrame


def WeightAreas(thisFrame, basket):
    #This function goes through all player rebound areas and weights them based on how far the player is from the hoop.
    #INPUT: Frame at which to figure out areas & the offensive basket the team is shooting at.
    #OUTPUT: Returns the frame with the weighted rebound values stored.
    

    for player in thisFrame.teamHome.playersOnTeam: #86138
        distanceToBasket = math.sqrt(math.pow(player.x - basket.x, 2) + math.pow(player.y - basket.y, 2))
        player.attributes.distanceFromBasket = distanceToBasket
        reboundProb = player.reboundArea / max(abs(distanceToBasket - 5), 1) #Hardcoding 5 feet from basket as optimal rebound distance.  Eventually this should be replaced by formal model.
        player.reboundProb = reboundProb

        #plt.plot(player.x, player.y)
        #plt.text(player.x, player.y, player.reboundProb)
        

    for player in thisFrame.teamAway.playersOnTeam:
        distanceToBasket = math.sqrt(math.pow(player.x - basket.x, 2) + math.pow(player.y - basket.y, 2))
        player.attributes.distanceFromBasket = distanceToBasket
        reboundProb = player.reboundArea / max(abs(distanceToBasket - 5), 1) #Hardcoding 5 feet from basket as optimal rebound distance.  Eventually this should be replaced by formal model.
        player.reboundProb = reboundProb


        #plt.plot(player.x, player.y)
        #plt.text(player.x, player.y, player.reboundProb)

    #plt.show()
    return thisFrame


if __name__ == '__main__':
    
    SetGlobalCandidateThresholds()
    InitializeGameInfo()
  
    LoadTrackingGameData()
    #CalculateReboundsFromPickle()
