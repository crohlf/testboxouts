'''
Created on June 19, 2018

@author: Charlie Rohlf
'''

class ReboundDataAttributes:
    
   
    def __init__(self):

        self._listPlayersFromBasketatShot = []
        self._listPlayersFromBasketatReb = []
        self._listPlayersFromRebLoc = []
        self._numberOfHomePlayersTryingForReb = 0
        self._numberOfAwayPlayersTryingForReb = 0
        self._numberOfHomePlayersCrashing = 0
        self._numberOfAwayPlayersCrashing = 0
        self._numberOfOffPlayersTryingForReb = 0
        self._numberOfDefPlayersTryingForReb = 0
        self._numberOfOffPlayersCrashing = 0
        self._numberOfDefPlayersCrashing = 0
        self._numberOfOffPlayersGettingBack = 0
        self._numberOfDefPlayersLeakingOut = 0
        self._numberOfHomePlayersInTransition = 0
        self._numberOfAwayPlayersInTransition = 0
        self._wasReboundedInOppArea = False

class ReboundPlayerAttributes:

    def __init__(self):

        self._changeInSquareFootageControlled = 0.0 #Reb
        self._gotReboundinOpponentArea = False #Reb
        self._distanceTravelledToGetRebound = 0.0 #Reb
        self._triedForRebound = False #Reb
        self._crashedForRebound = False #Reb
        self._distanceToReboundLocation = 0.0 #Shot
        self._numberOfCloserOpponents = 0 #Shot
        self._angleFromReboundLocation = 0.0 #Both
        self._changeInXDuringRebound = 0.0 #Reb
        self._inTransitionDuringRebound = False #Reb

