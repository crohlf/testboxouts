'''
Created on Jan 25, 2018

@author: MAmeer
'''
from BallPackage import Ball
from TeamPackage import Team
import sys
class Frame(object):
    #timeStamp = 0
    #ballAtThisFrame = Ball()
    #teamHome = Team()
    #teamAway = Team()
    
    def __init__(self, timeStamp = None, frameID = None, ballAtThisFrame = None, teamHome = None, teamAway = None, gameClock = None, period = None):
        self.timeStamp = timeStamp
        self.frameID = frameID
        self.ballAtThisFrame = ballAtThisFrame
        self.teamHome = teamHome
        self.teamAway = teamAway
        self.HomeBasket = None
        self.AwayBasket = None
        self.possessionTeam = None
        self.gameClock = gameClock
        self.period = period
    
    
    ''' returns timestamp of frame '''    
    def getTimeStamp(self):
        return self.timeStamp
    
    
    ''' returns Ball Position at this frame '''
    def getBallPosition(self):
        return self.ballAtThisFrame.getBallPosition()
    
    
    ''' returns home team '''
    def getHomeTeam(self):
        return self.teamHome
    
    
    ''' returns away team '''
    def getAwayTeam(self):
        return self.teamAway
    
    
    def getPlayer(self, playerID):
        playersOnCourt = self.getAllPlayers()
        for player in playersOnCourt:
            if player.playerId == playerID:
                return player
        return -1
    
        
    def getAllPlayers(self):
        a = self.getAwayTeam().getPlayers()
        b = self.getHomeTeam().getPlayers()
        return list(set(a).union(b))
    
    
    '''
    There may be an issue with Basket position not being loaded for some reason ---- test this at some point
    
    '''
    def getBasketPosition(self, side):
        if side == 0:
            try:
                return [self.HomeBasket.x, self.HomeBasket.y]
            except:
                print(self.frameID)
                return [0,0]
        elif side == 1:
            try:
                return [self.AwayBasket.x, self.AwayBasket.y]
            except:
                print(self.frameID)
                return [0,0]
        else:
            return [0,0]
        
    
    
    