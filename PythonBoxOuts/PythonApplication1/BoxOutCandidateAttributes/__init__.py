
'''
Created on May 11, 2018

@author: Charlie Rohlf
'''

import math

class BoxOutCandidateAttributes:
    

    def __init__(self, playerSumDistance, homePlayerSumSpeed, awayPlayerSumSpeed, wrestlingTime, firstCollisionFrame):

        self.totalDistanceBetweenPlayers = playerSumDistance
        self.wrestlingTime = wrestlingTime
        self.minimumDistanceBetweenPlayers = playerSumDistance
        self.homePlayerTotalAccumulatedSpeedDuringWrestling = homePlayerSumSpeed
        self.awayPlayerTotalAccumulatedSpeedDuringWrestling = awayPlayerSumSpeed
        self.homePlayerMaxAcceleration = -math.inf
        self.awayPlayerMaxAcceleration = -math.inf
        self.homePlayerMaximumSpeed = -math.inf
        self.awayPlayerMaximumSpeed = -math.inf
        self.collisionFrame = firstCollisionFrame
        self.distanceBetweenPlayersList = []