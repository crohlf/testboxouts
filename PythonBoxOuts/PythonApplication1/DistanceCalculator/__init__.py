'''
Created on Mar 5, 2018

@author: MAmeer
'''

import math
import numpy

#Get Distance between two positions. Accepts 2-3 dimensions, returns -1 otherwise
def getDistance3D(positionA, positionB):
    lenA = len(positionA)
    lenB = len(positionB)
    
    if lenA != lenB or (lenA > 3 or lenB > 3):
        return -1
    else:
 
        a = numpy.array(positionA)
        b = numpy.array(positionB)
        return numpy.linalg.norm(a - b)


def getDistance2D(positionA, positionB):
    lenA = len(positionA)
    lenB = len(positionB)
    
    if lenA != lenB or (lenA > 3 or lenB > 3):
        return -1
    else:
        return math.hypot(positionA[0] - positionB[0], positionA[1] - positionB[1])
        

def CalculatePlayerToOpponentAndBallDistance(ListOfFrames):
    for frame in ListOfFrames:
        CalculatePlayerToOpponentAndBallDistanceSingleFrame(frame)

        

def CalculatePlayerToOpponentAndBallDistanceSingleFrame(oneFrame, basket):

    for homePlayer in oneFrame.teamHome.playersOnTeam:

        homePlayer.setDistanceFromBall(oneFrame.getBallPosition())
        homePlayer.setDistanceFromOtherPlayers(oneFrame.teamAway.playersOnTeam)
        homePlayer.setDistanceFromBasket(basket.getBasketPosition())

    for awayPlayer in oneFrame.teamAway.playersOnTeam:

        awayPlayer.setDistanceFromBall(oneFrame.getBallPosition())
        awayPlayer.setDistanceFromOtherPlayers(oneFrame.teamHome.playersOnTeam)
        awayPlayer.setDistanceFromBasket(basket.getBasketPosition())

             
def BallToBasketDistance(ListOfFrames):
    for frame in ListOfFrames:
        if frame.HomeBasket is None:
            pass
            #print("Basket Data Not Found at: ", frame.frameID)
        else:
            distanceToHomeBasket = getDistance3D(frame.ballAtThisFrame.getBallPosition(), frame.HomeBasket.getBasketPosition())
            distanceToAwayBasket = getDistance3D(frame.ballAtThisFrame.getBallPosition(), frame.AwayBasket.getBasketPosition())
        
        
        frame.ballAtThisFrame.attributes.distanceFromHomeBasket = distanceToHomeBasket
        frame.ballAtThisFrame.attributes.distanceFromAwayBasket = distanceToAwayBasket
                
                
                