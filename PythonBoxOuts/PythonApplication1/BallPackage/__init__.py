import math
'''
Created on Jan 25, 2018

@author: MAmeer
'''
from BallAttributesPackage import BallAttributes

class Ball(object):
    #x = 0.0
    #y = 0.0
    #z = 0.0
    
    def __init__(self, x = None, y = None, z = None):
        self.x = x
        self.y = y
        self.z = z
        self.attributes = BallAttributes()
    
    def getBallPosition(self):
        return [self.x ,self.y, self.z]
    
    
    def setBallAttributes(self, field, data):
        try:
            self.attributes.setAttributes(field, data)
        except:
            print('No attribute object')
    
        
        