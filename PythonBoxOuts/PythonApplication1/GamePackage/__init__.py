'''
Created on Feb 13, 2018

@author: MAmeer
'''

class Game:
    
    def __init__(self, gameID = None, gamePeriods = None):
        
        self.gameID = gameID
        
        if gamePeriods is None:
            pass
        else:
            self.gamePeriods = gamePeriods
      
   
    ''' Return gameID '''        
    def getGameID(self):
        return self.gameID 
    
    '''return Periods '''
    def getPeriodData(self):
        return self.gamePeriods       