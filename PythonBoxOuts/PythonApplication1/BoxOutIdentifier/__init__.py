'''
Created on Apr 20, 2018

@author: MAmeer

Box Out Identifier: Finds all instances of potential box outs and stores the related meta data into a table

'''

def __init__(self, Eventsdata = None):
    
    #Check for empty Events
    if Eventsdata == None:
        print("No Event Data Given")
        return -1

    #Temp snippet list and list of possible boxouts
    tempSnip = []
    possibleBoxOuts = []
    
    tmpShot = None
    tmpReb = None

    #Search for data between shot and rebound events
    for event in Eventsdata:
        if event.eventType == 'SHOT' and tmpShot == None:
            tmpShot = event
        
        if event.eventType == 'REB' and tmpReb == None:
            tmpReb = event

        if tmpShot != None and tmpReb != None:
            tempSnip = [tmpShot, tmpReb]
            tmpShot = None
            tmpReb = None

