import math
'''
Created on Jan 25, 2018

@author: MAmeer
'''
class Ball(object):
    #x = 0.0
    #y = 0.0
    #z = 0.0
    
    def __init__(self, x = None, y = None, z = None):
        self.x = x
        self.y = y
        self.z = z
    
    def getBallPosition(self):
        return [self.x ,self.y, self.z]
    
    def distanceFromBasket(self, basket):
        if basket is None:
            print("No basket found")
            return -1
        else:
            return math.hypot(basket.x - self.x, basket.y - self.y)