''' 
Author: Joe Boyle
Create Date: 2/2/2018
Description: Loads in database configuration json and builds configuration list

sample json config

{
    "dbConfigs": [
        {
            "name": "CoreStatsQA",
            "driver": "Adaptive Server Enterprise",
            "server": "njdsybdb1-new",
            "database": "db_NBA_CoreStats_QA",
            "port": "4300",
            "trustedConnection": "yes",
			"dsn": "SybaseQA",
			"uid": "statsmonitorws",
			"pwd": "statsmonitorws"
        },
        {
            "name": "ConnectionName2",
            "driver": "SQL Server Native Client 11.0",
            "server": "ServerName",
            "database": "DatabaseName",
            "port": 1433,
            "trustedConnection": "yes",
			"dsn": "DSN2",
			"uid": "uid",
			"pwd": "pwd"			
        }
    ]
}

'''
import json
import sys
from DBConfigsPackage import dbConfig


class dbConfigList:   

    # configList = []

    # loads the configurations from the file
    def loadJsonConfig(self, filePath):
        # declare list of db configurations
        configList = []

        try:
            # load configurations from json file
            with open(filePath, 'r') as f:
                configs = json.load(f)
                # create dbConfig for each entry in file, add to the list
                for config in configs['dbConfigs']:
                    # declare instance of dbConfig
                    c = dbConfig.dbConfig(config.get('name'), config.get('driver'), config.get('server'), config.get('database'), config.get('trustedConnection'), config.get('port'), config.get('dsn'), config.get('uid'), config.get('pwd'))
                    # add config to list
                    configList.append(c)
        except:
            print("Error: ", sys.exc_info()[0])

        # return list        
        return configList

    # returns a configuration from a given name
    def getConfigByName(self, connectionName, configList):
        config = dbConfig.dbConfig('', '', '', '', '', 0, '', '', '')

        try:
            for c in configList:
                if c.dbName == connectionName:
                    config = c
        except:
            print("Error: ", sys.exc_info()[0])
        
        return config
    
    def __init__(self, filePath=None):
        self.filePath = filePath
        # load configurations
        self.configList = self.loadJsonConfig(filePath)

