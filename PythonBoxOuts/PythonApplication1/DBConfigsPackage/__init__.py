
''' 
Author: Joe Boyle
Create Date: 2/20/2018
Description: Initializes a DB connection configuration and can Execute a SQL statement

requires inclusion of pyodbc module
https://github.com/mkleehammer/pyodbc


Test code:
    db = DBProvider('CoreStatsQA')
    cursor = db.ExecuteQuery("select * from game where game_id = '0021700001'", 'DSN')
    row = cursor.fetchone()
    print(row)


'''

import pyodbc # @UnresolvedImport
import os
import sys
from DBConfigsPackage import dbConfigList


class DBProvider:

    # executes a SQL statement for against the current connection configuration
    def ExecuteQuery(self, sql=None, connectType=None):

        if(connectType == 'DSN'):
            try:
                connString = "DSN=" + self.configuration.dsn + ";" + "UID=" + self.configuration.uid + ";" + "PWD=" + self.configuration.uid + ";"
                # Connect via DSN
                conn = pyodbc.connect(connString)
                cursor = conn.cursor()
                cursor.execute(sql)
            except pyodbc.Error as e:
                print("Error connecting to database: ", e.args[1])
            except:
                print("Error: ", sys.exc_info()[0])
        elif (connectType == 'DRIVER'):
            try:
                # Connect via driver attributes
                connString = "DRIVER=" + self.configuration.driver + ";SERVER=" + self.configuration.server + ";PORT=" + self.configuration.port + ";DATABASE=" + self.configuration.database + ";UID=" + self.configuration.uid + ";PWD=" + self.configuration.pwd
                conn = pyodbc.connect(connString)
                cursor = conn.cursor()
                cursor.execute(sql)
            except pyodbc.InterfaceError as e:
                print("Error connecting to database: ", e)
            except:
                print("Error: ", sys.exc_info()[1])
        return cursor

    def __init__(self, connectionName=None):
        self.filePath = os.path.dirname(os.path.realpath(__file__)) + '\config.json'
        configObj = dbConfigList.dbConfigList(self.filePath)
       
        configList = configObj.configList
        # self.configuration = self.configList.index(connectionName)
        self.configuration = configObj.getConfigByName(connectionName, configList)
