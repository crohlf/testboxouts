''' 
Author: Joe Boyle
Create Date: 2/2/2018
Description: Class to contain a single database configuration

'''


class dbConfig:

    def __init__(self, _dbName, _driver, _server, _database, _trustedConnection, _port, _dsn, _uid, _pwd):
        self.dbName = _dbName
        self.driver = _driver
        self.server = _server
        self.database = _database
        self.trustedConnection = _trustedConnection
        self.port = _port        
        self.dsn = _dsn
        self.uid = _uid
        self.pwd = _pwd
