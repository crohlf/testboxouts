
'''
Created on April 24, 2018

@author: Charlie Rohlf
'''
from FramePackage import Frame
from ReboundDataAttributes import ReboundDataAttributes

class ReboundData(object):

    
    def __init__(self, shotEvent, rebEvent, shotFrame, rebFrame):
        self._shotFrame = shotFrame
        self._rebFrame = rebFrame
        self._shotEvent = shotEvent
        self._rebEvent = rebEvent
        self._attributes = ReboundDataAttributes()       