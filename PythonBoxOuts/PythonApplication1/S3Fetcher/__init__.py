

'''
Created on Apr 18, 2018

@author: MAmeer

Amazon S3 Bucket Data Fetcher: Retrieves data from the Second Spectrum S3 Bucket

'''

import boto3
import json
import gzip
import io
import shutil
from JSONLoaderPackage import JSON_Loader

def getTrackingFileFromS3(key):

    bucketName = 'ssi-us-west-2-nba-export-tracking'
    
    s3 = boto3.client('s3', aws_access_key_id = 'AKIAITSD2TC73YIBGLJA', aws_secret_access_key= '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')
    
    try:
        s3.download_file(bucketName, key + '.gz', 'tmpTrackingS3.jsonl.gz')

        fIn = gzip.open('tmpTrackingS3.jsonl.gz', 'rb')
        fOut = open('TrackingData.jsonl', 'wb')
        shutil.copyfileobj(fIn, fOut)

    except:
        s3.download_file(bucketName, key, 'TrackingData.jsonl')

        return 'TrackingData.jsonl'


def getEventsFileFromS3(key):

    bucketName = 'ssi-us-west-2-nba-export-events-nba'
    
    s3 = boto3.client('s3', aws_access_key_id = 'AKIAITSD2TC73YIBGLJA', aws_secret_access_key= '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')
    
    try:
        s3.download_file(bucketName, key + '.gz', 'tmpEventsS3.jsonl.gz')

        fIn = gzip.open('tmpEventsS3.jsonl.gz', 'rb')
        fOut = open('EventsData.jsonl', 'wb')
        shutil.copyfileobj(fIn, fOut)

    except:
        s3.download_file(bucketName, key, 'EventsData.jsonl')

        return 'EventsData.jsonl'


def getPossessionsFileFromS3(key):

    bucketName = 'ssi-us-west-2-nba-export-possessions'

    s3 = boto3.client('s3', aws_access_key_id = 'AKIAITSD2TC73YIBGLJA', aws_secret_access_key= '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')
    
    try:
        s3.download_file(bucketName, key + '.gz', 'tmpPossessionsS3.json.gz')

        fIn = gzip.open('tmpPossessionsS3.json.gz', 'rb')
        fOut = open('PossessionsData.json', 'wb')
        shutil.copyfileobj(fIn, fOut)

    except:
        s3.download_file(bucketName, key, 'PossessionsData.json')

        return 'PossessionsData.json'
