'''
Created on Feb 20, 2018

@author: MAmeer
'''

from ReboundDataAttributes import ReboundPlayerAttributes

class PlayerAttributes:
    
    '''
    mapping - dictionary which enumerates each type of attribute
    attributes - dictionary of attributes for this specific player, each attribute has its own 
                 dictionary containing the details of that respective attribute
    
    '''
    def __init__(self):

        self.distanceFromBasket = 0.0
        self.distanceFromBall = 0.0
        self.distanceFromOpponents = []
        self.angleFromBasket = 0.0
        self.speed = 0.0
        self.acceleration = 0.0
        self.directionOfMovement = 0.0
        self.iHaveBall = False
        self.acceleration = 0.0
        self.reboundAttributes = ReboundPlayerAttributes()
    
    
    
    
    
    