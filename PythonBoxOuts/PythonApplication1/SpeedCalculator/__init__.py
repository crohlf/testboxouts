
import math

#Range is distance from frame in one direction
def CalculateMomentSpeeds(ListOfFrames, framerange):
    fastestSpeed = 0
    #Calculates Speed Attribute for every player in every frame
    for index, frame in enumerate(ListOfFrames):  
        
        previousFrame = frame
        playersInFrame = frame.getAllPlayers()
        nextFrame = frame
         
        if (index - framerange) < 0:
            nextFrame = ListOfFrames[index + framerange]
            print('index - range out of bounds')
        
        else:
            if (index + framerange + 1) > len(ListOfFrames):
                previousFrame = ListOfFrames[index - framerange]
                print('index + range out of bounds')
            
            else:
                previousFrame = ListOfFrames[index - framerange]
                nextFrame = ListOfFrames[index + framerange]
        
        
        for player in playersInFrame:

            previousPlayer = previousFrame.getPlayer(player.playerId)
            nextPlayer = nextFrame.getPlayer(player.playerId)
            
            if nextPlayer == -1:
                nextFrameXY = player.getPosition()
                nextFrameTime = frame.timeStamp
            else:
                nextFrameXY = nextPlayer.getPosition()
                nextFrameTime = nextFrame.timeStamp
            
            if previousPlayer == -1:
                previousFrameXY = player.getPosition()
                previousFrameTime = frame.timeStamp
            else:    
                previousFrameXY = previousPlayer.getPosition()
                previousFrameTime = previousFrame.timeStamp
            
            
            positionDiff = math.hypot(nextFrameXY[0] - previousFrameXY[0], nextFrameXY[1] - previousFrameXY[1])
            timeDiff = nextFrameTime - previousFrameTime + 1 #No divide by zero
                    
            speed = abs(positionDiff/timeDiff)*1000
            player.attributes.speed = speed

            if previousPlayer != -1: 
                previousSpeed = previousPlayer.attributes.speed
                speedDiff = speed - previousSpeed
                timeDiff = frame.timeStamp - previousFrameTime + 1
                acceleration = 1000*(speedDiff/abs(timeDiff))
            else:
                acceleration = -1


            player.attributes.acceleration = acceleration

