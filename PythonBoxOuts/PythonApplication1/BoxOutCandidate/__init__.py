'''
Created on Apr 20, 2018

@author: MAmeer

Box Out: Defines a Box Out object. This is flattened and exported as a CSV

'''

class BoxOutCandidate(object):

    def __init__(self, homePlayer, awayPlayer, startFrame, endFrame, shotFrame, possession, eventNum, attributes):
        self._shotFrame = shotFrame
        self._homePlayer = homePlayer
        self._awayPlayer = awayPlayer
        self._startFrame = startFrame
        self._endFrame = endFrame
        self._shotFrame = shotFrame
        self._possession = possession
        self._attributes = attributes
        self._shooterInovlved = 0
        self._rebounderInvolved = 0
        self._shotFrameEventNum = eventNum
        self._isLoggedAsBoxOut = 0
        self._isMadeFGA = 0
        self._toBeKeptInList = True

