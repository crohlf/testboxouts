'''
Created on Feb 13, 2018

@author: MAmeer
'''

from BasketsPackage import Basket

class Period:
    '''
    periodNumber = 0
    basketTeamHome = Basket()
    basketTeamAway = Basket()
    possessionsForPeriod = []
    '''
    
    def __init__(self, basketTeamHome = None, basketTeamAway = None, possessionsForPeriod = None ):
        self.basketTeamHome = basketTeamHome
        self.basketTeamAway = basketTeamAway
        
        if possessionsForPeriod is None:
            pass
        
        else:
            self.possessionsForPeriod = possessionsForPeriod