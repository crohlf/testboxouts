import math

#Range is distance from frame in one direction
def CalculateMomentSpeeds(ListOfFrames, framerange):
    fastestSpeed = 0
    #Calculates Speed Attribute for every player in every frame
    for index, frame in enumerate(ListOfFrames):  
        
        PreviousFrame = None  
        playersInFrame = frame.getAllPlayers()
        NextFrame = None
         
        if (index - framerange) < 0:
            NextFrame = ListOfFrames[index + framerange]
            print('index - range out of bounds')
        
        else:
            if (index + framerange + 1) > len(ListOfFrames):
                PreviousFrame = ListOfFrames[index - framerange]
                print('index + range out of bounds')
            
            else:
                PreviousFrame = ListOfFrames[index - framerange]
                NextFrame = ListOfFrames[index + framerange]
        
        speedlist = []
        for player in playersInFrame:
            
            if NextFrame is None or NextFrame.getPlayer(player.playerId) == -1:
                NextFrameXY = frame.getPlayer(player.playerId).getPosition()
                NextFrameTime = frame.timeStamp
            else:
                NextFrameXY = NextFrame.getPlayer(player.playerId).getPosition()
                NextFrameTime = NextFrame.timeStamp
            
            if PreviousFrame is None or PreviousFrame.getPlayer(player.playerId) == -1:
                PreviousFrameXY = frame.getPlayer(player.playerId).getPosition()
                PreviousFrameTime = frame.timeStamp
            else:    
                PreviousFrameXY = PreviousFrame.getPlayer(player.playerId).getPosition()
                PreviousFrameTime = PreviousFrame.timeStamp
            
            
            positionDiff = math.hypot(NextFrameXY[0] - PreviousFrameXY[0], NextFrameXY[1] - PreviousFrameXY[1])
            timeDiff = NextFrameTime - PreviousFrameTime
                    
            speed = abs(positionDiff/timeDiff)*1000
            #print("speed =", speed)
            player.attributes.speed = (speed)
    print("Speed Calculated")
    
    fastestAccelerationGame = 0.0
    #Calculates Acceleration for every player in every frame based on speed calc
    for index, frame in enumerate(ListOfFrames):  
        
        PreviousFrame = None  
        playersInFrame = frame.getAllPlayers()
        NextFrame = None
         
        if (index - framerange) < 0:
            NextFrame = ListOfFrames[index + framerange]
            print('index - range out of bounds')
        
        else:
            if (index + framerange + 1) > len(ListOfFrames):
                PreviousFrame = ListOfFrames[index - framerange]
                print('index + range out of bounds')
            
            else:
                PreviousFrame = ListOfFrames[index - framerange]
                NextFrame = ListOfFrames[index + framerange]
        
        accelList = []
        fastestAccelerationFrame = 0.0
        globalacceleration = 0.0
        for player in playersInFrame:
            
            if NextFrame is None or NextFrame.getPlayer(player.playerId) == -1:
                NextFrameSpeed = frame.getPlayer(player.playerId).attributes.speed
                NextFrameTime = frame.timeStamp
            else:
                NextFrameSpeed = NextFrame.getPlayer(player.playerId).attributes.speed
                NextFrameTime = NextFrame.timeStamp

            if PreviousFrame is None or PreviousFrame.getPlayer(player.playerId) == -1:
                PreviousFrameSpeed = frame.getPlayer(player.playerId).attributes.speed
                PreviousFrameTime = frame.timeStamp
            else:    
                PreviousFrameSpeed = PreviousFrame.getPlayer(player.playerId).attributes.speed
                PreviousFrameTime = PreviousFrame.timeStamp
                
            speedDiff = NextFrameSpeed - PreviousFrameSpeed
            timeDiff = NextFrameTime - PreviousFrameTime
            acceleration = 1000*(speedDiff/abs(timeDiff))

            player.attributes.acceleration = (acceleration)
            #print("acceleration = ", acceleration, "playerid = ", player.playerId)
            #print("time = ", frame.gameClock)

    print("done")
            