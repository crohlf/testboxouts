'''
Created on Jan 25, 2018

@author: CRohlf
'''
import json
from collections import OrderedDict


class JSON_Loader():
    '''
    classdocs
    '''
    def LoadJSONL(self, filePath):
        allLines = []
        with open(filePath) as jsonl_file:
            for line in jsonl_file:
                data = json.loads(line, object_pairs_hook=OrderedDict) 
                allLines.append(data)
                
        return allLines

    def LoadJSON(self, filePath):
        with open(filePath) as json_file:
            data = json.load(json_file)
    
        return data


    def __init__(self):
        '''
        Constructor
        '''
        