'''
Created on June 20, 2018

@author: Charlie Rohlf
'''

import math
import numpy

#Get 360 degree angle between two positions.
def getAngleBetweenTwoPoints(myPosition, destinationPosition):
    lenA = len(myPosition)
    lenB = len(destinationPosition)
    
    if lenA != lenB or (lenA > 3 or lenB > 3):
        return -1
    else:
        radians = math.atan2(myPosition[1] - destinationPosition[1], myPosition[0] - destinationPosition[0]) + 0.001 #To avoid very rare 0 case
        if radians > 0:
            degrees = radians * (180/math.pi)
        else:
            degrees = radians * (180/math.pi) + 360
            
        return degrees
