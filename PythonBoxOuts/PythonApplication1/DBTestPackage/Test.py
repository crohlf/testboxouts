''' 
Author: Joe Boyle
Create Date: 2/21/2018
Description: Tests database connection and query execution

'''
from DBConfigsPackage import DBProvider
from HustleStatsRetrievalPackage import HustleStatRetrieval
import sys

if __name__ == '__main__':
    db = DBProvider('CoreStatsQA')
    #print(db.filePath)
    #print(db.configuration)
    #cursor = db.ExecuteQuery("select * from game where game_id = '0021700001'", 'DSN')
    #row = cursor.fetchone()
    #print(row)
    #cursor = db.ExecuteQuery("select * from game where game_id = '0021700001'", 'DRIVER')
    #row = cursor.fetchone()
    #print(row)

    db = DBProvider('CoreStats')
    #print(sys.executable)
    #print(db.filePath)
    #print(db.configuration)
    cursor = db.ExecuteQuery("Select Event_Num from dbo.Game_Event where game_id = '0021700757' and Event_Msg_Type = 3", 'DRIVER')
    
    listOfAllFTs = []

    for row in cursor.fetchall():
        listOfAllFTs.append(row[0])
    row = cursor.fetchone()
    print(row)

    print("***********************************")
    hustle = HustleStatRetrieval('CoreStats')
    hustleEvents = hustle.GetHustleStatsForGame("0021700757")

    for h in hustleEvents:
        print(h.gameId, ", ", h.eventNum, ", ", h.gameClock, ", ", h.playerName, ", ", h.teamName, ", ", h.hustleStatType.name)

    print("***********************************")
    hustleTypeEvents = hustle.GetHustleStatForGameByType("0021700757", "BOX_OUT")

    for h in hustleTypeEvents:
        print(h.gameId, ", ", h.eventNum, ", ", h.gameClock, ", ", h.playerName, ", ", h.teamName, ", ", h.hustleStatType.name)

    '''
    while True:
        row = cursor.fetchone()
        if(row == None):
            break
        print(row)
    '''

