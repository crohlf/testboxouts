''' 
Author: Charlie Rohlf
Create Date: 4/16/2018
Description: Class to write models to disk

'''

import pickle
import sys

class ModelWriter:

    def WriteModelToDisk(self):

        try:

            with open(self.filename, 'wb') as filetowrite:
                pickle.dump(self.model, filetowrite)
                filetowrite.close()

        except:
            print("Error: ", sys.exc_info()[0])

    def __init__(self, _filename, _model):
        self.filename = _filename
        self.model = _model
