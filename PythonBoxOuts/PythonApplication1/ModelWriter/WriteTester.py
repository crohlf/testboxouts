''' 
Author: Charlie Rohlf
Create Date: 4/20/2018
Description: Code to test ModelWriter class

'''
from ModelWriter import ModelWriter
from ModelReader import ModelReader
import sklearn
from sklearn import svm, datasets
import sys

if __name__ == '__main__':

    iris = datasets.load_iris()

    X = iris.data[:, :2]
    y = iris.target
    
    csvData = []
    targets = []
    
    X = [[0, 0], [1, 1]]
    y = [0, 2]
    clf = svm.SVC(C=1.0, cache_size=200, class_weight=None, coef0=0.0,
    decision_function_shape='ovr', degree=3, gamma='auto', kernel='rbf',
    max_iter=-1, probability=True, random_state=None, shrinking=True,
    tol=0.001, verbose=False)
    clf.fit(X, y)  
    svm.SVC(C=1.0, cache_size=200, class_weight=None, coef0=0.0,
    decision_function_shape='ovr', degree=3, gamma='auto', kernel='rbf',
    max_iter=-1, probability=True, random_state=None, shrinking=True,
    tol=0.001, verbose=False)

    valueToTest = [[1000., 10.]]

    preWriteValue = clf.predict_proba(valueToTest)

    myFileName = "C:\TrackingData\Picklesave2"

    myWriter = ModelWriter(myFileName, clf)
    myWriter.WriteModelToDisk()

    try:
        
        myReader = ModelReader(myFileName)
        myModel = myReader.ReadModelFromDisk()
        
        if not isinstance(myModel, svm.classes.SVC):
            raise Exception("Did not get back SVM Type")
    
    except:

        print("Error: ", sys.exc_info()[0])

    postWriteValue = myModel.predict_proba(valueToTest)

    if preWriteValue[0][0] == postWriteValue[0][0]:
        print("SUCCESS! Values Match Before & After Writing File")

