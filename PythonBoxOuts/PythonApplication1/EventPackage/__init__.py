'''
Created on Feb 16, 2018

@author: MAmeer
'''
class Event(object):
    
    '''
    eventType
    timeStamp
    possessionId
    
    '''
    
    def __init__(self, eventData):
        
        self.eventType = eventData['eventType']
        self.timeStamp = eventData['wallClock']
        self.possessionID = eventData['possessionId']
        self.eventNum = eventData['pbpId']
        self.shotClockTime = eventData['shotClock']
        self.gameClock = eventData['gameClock']
        self.playerId = list(eventData['event'].values())[0]
        self.teamId = eventData['event']['teamId']
        
        if 'location' in eventData:
            self.location = eventData['location']
    
    def getEventPossessionID(self):
        return self.possessionID
        